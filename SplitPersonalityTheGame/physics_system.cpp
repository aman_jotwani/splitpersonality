struct Physics_System
{
	static const float Timestep;
	static void Integrate(PhysicsData* State, vec2 (*Acc)(const PhysicsData&));
};

const float Physics_System::Timestep = 0.1f;

struct Derivative
{
	vec2 dpos;
	vec2 dvel;
};

internal
Derivative InitialEvaluate(const PhysicsData& initial, vec2 (*AccFuncPtr)(const PhysicsData&))
{
	Derivative output = {0};
	output.dpos = initial.Velocity;
	output.dvel = AccFuncPtr(initial);
	return output;
}

internal
Derivative Evaluate(const PhysicsData& initial, float dt, Derivative& d, vec2 (*AccFuncPtr)(const PhysicsData&))
{
	PhysicsData s;
	s.Position = initial.Position + d.dpos*dt;
	s.Velocity = initial.Velocity + d.dvel*dt;

	Derivative output = {0};
	output.dpos = s.Velocity;
	output.dvel = AccFuncPtr(initial);
	return output;
}

// function pointer for force function
void 
Physics_System::Integrate(PhysicsData* state, vec2 (*AccFuncPtr)(const PhysicsData&))
{
	PhysicsData initial_state = *state;

	Derivative a = InitialEvaluate(initial_state, AccFuncPtr);
	Derivative b = Evaluate(initial_state, Timestep*0.5f, a, AccFuncPtr);
	Derivative c = Evaluate(initial_state, Timestep*0.5f, b, AccFuncPtr);
	Derivative d = Evaluate(initial_state, Timestep, c, AccFuncPtr);

	vec2 dposdt = (a.dpos + (b.dpos + c.dpos)*2.0f + d.dpos)*(1.0f/6.0f);
	vec2 dveldt = (a.dvel + (b.dvel + c.dvel)*2.0f + d.dvel)*(1.0f/6.0f);
    
    (*state).Position += dposdt*Timestep;
    (*state).Velocity += dveldt*Timestep;
}