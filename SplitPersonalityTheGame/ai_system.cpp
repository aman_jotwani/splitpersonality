enum Difficulty
{
	Beginner,
	BeginnerTest, // peak
	Intermediate,
	IntermediateTest, // peak
	Hard,
	HardTest, // peak and rest
	EndSequence // peak and end
};

struct TuningParams
{
	uint32_t IntervalLowerLimit;
	uint32_t IntervalUpperLimit;
	uint32_t PeakLowerLimit;
	uint32_t ClimbRate;
	uint32_t MaxEnemies;
	uint32_t EnemiesKilled;
	uint32_t Target;
	bool     ChargingEnabled;
	bool     GoombasEnabled;
	bool     ChargingGoombasEnabled;
};

struct Ai_System
{
	Uint32 SpawnCounter;
	Uint32 SpawnInterval;
	Uint32 LowerLimit;
	Uint32 UpperLimit;
	TuningParams CurrParams;
	TuningParams BeginnerParams;
	TuningParams BeginnerTestParams;
	TuningParams IntermediateParams;
	TuningParams IntermediateTestParams;
	TuningParams HardParams;
	TuningParams HardTestParams;

	Difficulty Progression;
	bool GameOver;

	Ai_System();
	void Init();
	void Think(uint32_t ArrowsHeld, Enemy_System* Enemies, Powerup_System* Powerups);
	void Tune();
};

Ai_System::Ai_System()
{
	GameOver = false;
}

void
Ai_System::Init()
{
	SpawnCounter = SDL_GetTicks();
	GameOver = false;
	
	BeginnerParams = {2000, 4000, 1500, 100, 5, 0, 15, false, false, false};

	BeginnerTestParams = {1500, 3000, 500, 100, 8, 0, 10, false, false, false};

	IntermediateParams = {2000, 4000, 1500, 100, 5, 0, 15, false, false, false};

	IntermediateTestParams = {1500, 3000, 500, 100, 10, 0, 10, false, false, false};

	HardParams = {2000, 4000, 1500, 100, 5, 0, 5, false, true, false};

	HardTestParams = {1500, 3000, 500, 100, 10, 0, 5, false, true, false};

	CurrParams = BeginnerParams;
	Progression = Beginner;

	LowerLimit = CurrParams.IntervalLowerLimit;
	UpperLimit = CurrParams.IntervalUpperLimit;
}

void
Ai_System::Tune()
{

}

void
Ai_System::Think(uint32_t ArrowsHeld, Enemy_System* Enemies, Powerup_System* Powerups)
{
	uint32_t StdArrowEnemies = Enemies->GetNumArrowKillables();
	uint32_t StdSwordEnemies = Enemies->GetNumSwordKillables();
	uint32_t ChgEnemies = Enemies->GetNumChgEnemies();
	uint32_t GroundedArrows = Powerups->GetGroundedArrows();
	bool IsGoombaAlive = Enemies->Goomba.IsAlive;
	
	uint32_t NumEnemies = StdArrowEnemies + StdSwordEnemies;

	SpawnInterval = GetRandomInt(LowerLimit, UpperLimit);
	if(NumEnemies < CurrParams.MaxEnemies && GetMSElapsedSince(SpawnCounter) > SpawnInterval)
	{
		bool SpawnFromRight = RandomDecision();
		bool IsArrowKillable = RandomDecision();
		bool SpawningCharged = false;
		EnemyState EnemyType = IsArrowKillable ? ArrowKillable : SwordKillable;
		
		if(CurrParams.ChargingEnabled && ChgEnemies == 0 && !IsGoombaAlive && NumEnemies <= 1/* && some balancing bools */)
		{
			if(RandomDecision() && RandomDecision())
			{
				EnemyType = Charging;
			}
			Enemies->SpawnEnemy(SpawnFromRight, EnemyType);
		}
		if(CurrParams.GoombasEnabled /* && some balancing bools */)
		{
			bool SpawningGoomba = false;
			if(!IsGoombaAlive && !SpawningCharged && ChgEnemies == 0 && NumEnemies <= 1)
			{
				SpawningGoomba = true;
				Enemies->SpawnIntermediateGoomba(SpawnFromRight, false);
			}
		}
		if(!IsGoombaAlive && !SpawningCharged && ChgEnemies <= 1)
		{
			Enemies->SpawnEnemy(SpawnFromRight, EnemyType);			
		}

		if(LowerLimit != CurrParams.PeakLowerLimit)
		{
			LowerLimit -= CurrParams.ClimbRate;			
		}
		else
		{
			LowerLimit = CurrParams.IntervalLowerLimit;			
		}
		SpawnInterval = GetRandomInt(LowerLimit, UpperLimit);
		SpawnCounter = SDL_GetTicks();
	}

	local_persist Uint32 arrow_spawn_counter = SDL_GetTicks();
	if(GetMSElapsedSince(arrow_spawn_counter) > 2000 && 
	   (GroundedArrows + ArrowsHeld) <= 5)
	{
		Powerups->SpawnArrowMidair();
		arrow_spawn_counter = SDL_GetTicks();
	}

	if(CurrParams.EnemiesKilled > CurrParams.Target && !IsGoombaAlive)
	{
		switch(Progression)
		{
			case Beginner:
			{
				CurrParams = BeginnerTestParams;
				Progression = BeginnerTest;
				ConsoleLog("Beginning Test.");
			}
			break;

			case BeginnerTest:
			{
				CurrParams = IntermediateParams;
				Progression = Intermediate;
				ConsoleLog("Beginning Intermediate.");
			}
			break;

			case Intermediate:
			{
				CurrParams = IntermediateTestParams;
				Progression = IntermediateTest;
				ConsoleLog("Beginning IntermediateTest.");
			}
			break;

			case IntermediateTest:
			{
				CurrParams = HardParams;
				Progression = Hard;
				ConsoleLog("Beginning Hard.");			
			}
			break;

			case Hard:
			{
				CurrParams = HardTestParams;
				Progression = HardTest;
				ConsoleLog("Beginning HardTest.");				
			}
			break;

			case HardTest:
			{
				GameOver = true;
				// CurrParams = EndSequenceParams;
			}
			break;

			case EndSequence:
			{
				GameOver = true;
			}
			break;
		}
		LowerLimit = CurrParams.IntervalLowerLimit;
		UpperLimit = CurrParams.IntervalUpperLimit;
		SpawnCounter = SDL_GetTicks();
	}
}