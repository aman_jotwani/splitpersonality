struct ConfigData
{
	// New options need to be manually changed in Init.
	static const size_t s_NumFullscreenOptions = 2;
	static const size_t s_NumScreensizeOptions = 5;

	bool 	  Done;

	void Init(SDL_Renderer* Renderer);
	void HandleEvents(SDL_Renderer* Renderer, const SDL_Event& Event);
	void UpdateAndRender(PlatformData* Engine, ProgramState* GameState);
	void RenderUI(SDL_Renderer* Renderer);
	void Cleanup();

private:
	SDL_Color UIColor;
	TTF_Font* UIFont;
	Texture   UIFullscreenText;
	Texture   UIFullscreenOption;
	Texture   UILeftArrow;
	Texture   UIRightArrow;
	Texture   TextEnter;
	Texture   UIScreenSizeText;
	Texture   UIScreenSizeOption;
	char*     FullscreenOptions[s_NumFullscreenOptions];
	char*     ScreenSizeOptions[s_NumScreensizeOptions];
	size_t    CurrFullscreenOption;
	size_t    CurrScreenSizeOption;
	bool      CurrSetting;
};

void
ConfigData::Init(SDL_Renderer* Renderer)
{
	UIFont = TTF_OpenFont("..\\assets\\fonts\\walibi-holland.ttf", 64);
	UIColor = {0xFF, 0xFF, 0xFF, 0xFF};
	
	Load_SolidText(Renderer, "Fullscreen?", UIFont, UIColor, &UIFullscreenText);
	Load_SolidText(Renderer, "Resolution?", UIFont, UIColor, &UIScreenSizeText);
	Load_SolidText(Renderer, "Press Enter to play.", UIFont, UIColor, &TextEnter);

	LoadImage(Renderer, "..\\assets\\graphics\\left_arrow.png", &UILeftArrow);
	LoadImage(Renderer, "..\\assets\\graphics\\right_arrow.png", &UIRightArrow);
	
	FullscreenOptions[0] = "Fullscreen";
	Load_SolidText(Renderer, FullscreenOptions[0], UIFont, UIColor, &UIFullscreenOption);
	FullscreenOptions[1] = "Windowed";

	ScreenSizeOptions[0] = "1024x768";   // 4:3
	ScreenSizeOptions[1] = "1280x700";  // 16:9
	ScreenSizeOptions[2] = "1366x768";   // 16:9
	ScreenSizeOptions[3] = "1600x900";   // 16:9
	ScreenSizeOptions[4] = "1920x1080";  // 16:9
	// for debug, sensible default based on monitor should be used.
	Load_SolidText(Renderer, ScreenSizeOptions[4], UIFont, UIColor, &UIScreenSizeOption);

	CurrFullscreenOption = 0;
	CurrScreenSizeOption = 4;
	CurrSetting = true;
	Done = false;
}

void
ConfigData::HandleEvents(SDL_Renderer* Renderer, const SDL_Event& Event)
{
	if(Event.type == SDL_KEYDOWN)
	{
		switch(Event.key.keysym.sym)
		{
			case SDLK_DOWN: // controls which setting is being modified
			{
				CurrSetting = CurrSetting ? false : true;
			}
			break;

			case SDLK_UP:
			{
				CurrSetting = CurrSetting ? false : true;
			}
			break;

			case SDLK_RIGHT: // right and left, controls which variation of the current setting is being selected
			{
				if(CurrSetting)
				{
					if(CurrFullscreenOption < s_NumFullscreenOptions - 1)
					{
						CurrFullscreenOption += 1;
					}
					else
					{
						CurrFullscreenOption = 0;
					}

					SDL_DestroyTexture(UIFullscreenOption.Ptr);
					Load_SolidText(Renderer, FullscreenOptions[CurrFullscreenOption], UIFont, UIColor, &UIFullscreenOption);
				}
				else
				{
					if(CurrScreenSizeOption < s_NumScreensizeOptions - 1)
					{
						CurrScreenSizeOption += 1;
					}
					else
					{
						CurrScreenSizeOption = 0;
					}

					SDL_DestroyTexture(UIScreenSizeOption.Ptr);
					Load_SolidText(Renderer, ScreenSizeOptions[CurrScreenSizeOption], UIFont, UIColor, &UIScreenSizeOption);
				}
			}
			break;

			case SDLK_LEFT:
			{
				if(CurrSetting)
				{
					if(CurrFullscreenOption != 0)
					{
						CurrFullscreenOption -= 1;
					}
					else
					{
						CurrFullscreenOption = s_NumFullscreenOptions - 1;
					}

					SDL_DestroyTexture(UIFullscreenOption.Ptr);
					Load_SolidText(Renderer, FullscreenOptions[CurrFullscreenOption], UIFont, UIColor, &UIFullscreenOption);
				}
				else
				{
					if(CurrScreenSizeOption != 0)
					{
						CurrScreenSizeOption -= 1;
					}
					else
					{
						CurrScreenSizeOption = s_NumScreensizeOptions - 1;
					}

					SDL_DestroyTexture(UIScreenSizeOption.Ptr);
					Load_SolidText(Renderer, ScreenSizeOptions[CurrScreenSizeOption], UIFont, UIColor, &UIScreenSizeOption);
				}
			}
			break;

			case SDLK_RETURN: // to confirm config and move to gameplay
			{
				Done = true;
			}
			break;
		}
	}
}

void
ConfigData::UpdateAndRender(PlatformData* Engine, ProgramState* GameState)
{
	int StartX = 100;
	int StartY = 200;
	int HorizontalSpacing = 200;
	int VerticalSpacing = 200;
	int SelectorPadding = 10;

	SDL_SetRenderDrawColor(Engine->Renderer, 0xFF, 0x00, 0x00, 0xFF);
	if(CurrSetting)
	{	
		SDL_Rect Temp = {StartX - SelectorPadding, StartY - SelectorPadding, 
						 UIFullscreenText.DrawRect.w + HorizontalSpacing + UIFullscreenOption.DrawRect.w + 2*SelectorPadding,
						 UIFullscreenText.DrawRect.h};
		SDL_RenderDrawRect(Engine->Renderer, &Temp);
	}
	else
	{
		SDL_Rect Temp = {StartX - SelectorPadding, StartY + VerticalSpacing - SelectorPadding,
						 2*SelectorPadding + UIScreenSizeText.DrawRect.w + HorizontalSpacing + UIScreenSizeText.DrawRect.w,
						 UIScreenSizeText.DrawRect.h};
		SDL_RenderDrawRect(Engine->Renderer, &Temp);
	}
	SDL_SetRenderDrawColor(Engine->Renderer, 0x00, 0x00, 0x00, 0xFF);

	// option to switch to fullscreen
	// // draw ui text, highlight if currently selected
	// // show left arrow, draw current selection, show right arrow
	UIFullscreenText.Render(Engine->Renderer, StartX, StartY);
	UILeftArrow.Render(Engine->Renderer, StartX + UIFullscreenText.DrawRect.w + HorizontalSpacing/2, StartY);
	UIFullscreenOption.Render(Engine->Renderer, StartX + UIFullscreenText.DrawRect.w + HorizontalSpacing, StartY);
	UIRightArrow.Render(Engine->Renderer, StartX + UIFullscreenText.DrawRect.w + HorizontalSpacing + UIFullscreenOption.DrawRect.w, StartY);

	// give option to change between possible screen sizes
	// // draw ui text, highlight if selected
	// // arrows, current selection
	UIScreenSizeText.Render(Engine->Renderer, StartX, StartY + VerticalSpacing);
	UILeftArrow.Render(Engine->Renderer, StartX + UIScreenSizeText.DrawRect.w + HorizontalSpacing/2, StartY + VerticalSpacing);
	UIScreenSizeOption.Render(Engine->Renderer, StartX + UIScreenSizeText.DrawRect.w + HorizontalSpacing, StartY + VerticalSpacing);
	UIRightArrow.Render(Engine->Renderer, StartX + UIScreenSizeText.DrawRect.w + UIScreenSizeOption.DrawRect.w + HorizontalSpacing, 
						StartY + VerticalSpacing);

	TextEnter.Render(Engine->Renderer, StartX, StartY + 2*VerticalSpacing);
	if(Done)
	{
		if(CurrFullscreenOption == 0) // fullscreen
		{
			SDL_SetWindowFullscreen(Engine->Window, SDL_WINDOW_FULLSCREEN_DESKTOP);
		}
		else
		{
			SDL_SetWindowFullscreen(Engine->Window, 0);
		}

		switch(CurrScreenSizeOption)
		{
			case 0:
			{
				SDL_SetWindowSize(Engine->Window, 1024, 768);
				SDL_RenderSetLogicalSize(Engine->Renderer, 1024, 768);
				Engine->ScreenWidth = 1024;
				Engine->ScreenHeight = 768;
			}
			break;

			case 1:
			{
				SDL_SetWindowSize(Engine->Window, 1280, 700);
				SDL_RenderSetLogicalSize(Engine->Renderer, 1280, 700);
				Engine->ScreenWidth = 1280;
				Engine->ScreenHeight = 700;
			}
			break;

			case 2:
			{
				SDL_SetWindowSize(Engine->Window, 1366, 768);
				SDL_RenderSetLogicalSize(Engine->Renderer, 1366, 768);
				Engine->ScreenWidth = 1366;
				Engine->ScreenHeight = 768;
			}
			break;

			case 3:
			{
				SDL_SetWindowSize(Engine->Window, 1600, 900);
				SDL_RenderSetLogicalSize(Engine->Renderer, 1600, 900);
				Engine->ScreenWidth = 1600;
				Engine->ScreenHeight = 900;
			}
			break;

			case 4:
			{
				SDL_SetWindowSize(Engine->Window, 1920, 1080);
				SDL_RenderSetLogicalSize(Engine->Renderer, 1920, 1080);
				Engine->ScreenWidth = 1920;
				Engine->ScreenHeight = 1080;				
			}
			break;
		}
		(*GameState) = GAMEPLAY;
		Cleanup();
	}
}

void
ConfigData::Cleanup()
{
	TTF_CloseFont(UIFont);
	SDL_DestroyTexture(UIFullscreenText.Ptr);
	SDL_DestroyTexture(UIFullscreenOption.Ptr);
	SDL_DestroyTexture(UIScreenSizeText.Ptr);
	SDL_DestroyTexture(UIScreenSizeOption.Ptr);
	SDL_DestroyTexture(TextEnter.Ptr);
	SDL_DestroyTexture(UILeftArrow.Ptr);
	SDL_DestroyTexture(UIRightArrow.Ptr);
}