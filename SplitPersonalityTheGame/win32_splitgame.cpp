#include <stdint.h>

#define global static
#define local_persist static
#define internal static
#define ConsoleLog SDL_Log

#if defined(_MSC_VER)
	#define _CRT_RAND_S
	#include <stdlib.h>
	#include <string.h>
	#include <cmath>
#elif defined(__GNUC__) || defined(__GNUG__)
	#include <stdlib.h>
	#include <string.h>
	#include <cmath>
#endif 

#include "containers.h"
#include "sdl_utils.h" // sdl_utils.cpp separate translation unit, dependent on msvc crt because of _snprintf_s

// everything here onwards is single translation unit

enum ProgramState
{
	INITIAL_CONFIG_SCREEN,
	START_SCREEN,
	CONTROL_CONFIG_SCREEN,
	GAMEPLAY,
	PAUSE_SCREEN,
	GAME_OVER_SCREEN
};

global int g_RandomIntFails = 0;
global int g_ResolutionWidth = 0;
global int g_ResolutionHeight = 0;
global float g_HardwareTickTime = 0.0f;
global const float g_PhyScale = 0.1f;

global const uint32_t gScreenWidth = 1366;
global const uint32_t gScreenHeight = 768;

#include "win32_utils.cpp"   		 // game-independent utilities
#include "splitgame.h"       		// game-specific forward declarations for all systems

#include "config_ui.cpp"

#include "anim_system.cpp"
#include "physics_system.cpp"
#include "hud.cpp"
#include "player.cpp"
#include "weapons.cpp"
#include "enemies.cpp"
#include "ai_system.cpp"
// #include "wave_ai.cpp"
#include "gameplay_system.cpp"

int main(int argc, char** argv)
{
	PlatformData Engine;
	if(InitSDL(&Engine, "Split Personality", gScreenWidth, gScreenHeight))
	{
		bool running = true;
		bool paused = false;

		ProgramState GameState = INITIAL_CONFIG_SCREEN;

		SDL_Event e;

 		ConfigData InitialConfig;

 		Gameplay_System Gameplay;

 		InitialConfig.Init(Engine.Renderer);

		while(running)
		{
			while(SDL_PollEvent(&e) != 0)
			{
				if(e.type == SDL_QUIT || 
				   (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE))
				{
					running = false;
				}
				else if (GameState == INITIAL_CONFIG_SCREEN)
				{
					InitialConfig.HandleEvents(Engine.Renderer, e);
				}
				else if (GameState == START_SCREEN)
				{
				}
				else if (GameState == GAMEPLAY)
				{
					Gameplay.HandleEvents(e);
				}
				else if(GameState == GAME_OVER_SCREEN)
				{
					if(e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_r)
					{
						Gameplay.Init(Engine.Renderer);
						GameState = GAMEPLAY;
					}
				}
			}

			if(!paused)
			{
				if(GameState != GAME_OVER_SCREEN)
				{
					SDL_SetRenderDrawColor(Engine.Renderer, 0x00, 0x00, 0x00, 0xFF);
					SDL_RenderClear(Engine.Renderer);
				}

				if(GameState == INITIAL_CONFIG_SCREEN)
				{
					InitialConfig.UpdateAndRender(&Engine, &GameState);
					if(InitialConfig.Done)
					{
						// send config data to game
						g_ResolutionWidth = Engine.ScreenWidth;
						g_ResolutionHeight = Engine.ScreenHeight;
						Gameplay.Init(Engine.Renderer);
					}
				}
				else if(GameState == START_SCREEN)
				{
				}
				else if(GameState == GAMEPLAY)
				{
					Gameplay.UpdateAndRender();
					if(Gameplay.Player.HealthPoints <= 0)
					{
						GameState = GAME_OVER_SCREEN;
					}
				}
				
				if(GameState == GAME_OVER_SCREEN)
				{
				}
				SDL_RenderPresent(Engine.Renderer);
			}
		}
	}
	return 0;
}