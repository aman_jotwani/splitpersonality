struct FadeOutAnim
{
	Texture  Graphic;
	float    Alpha;
};

struct Anim_System
{
	static const uint32_t s_MaxAnims = 20;

	FadeOutAnim FadeOuts[s_MaxAnims];
	Anim_System();
	void AddFadeOut(SDL_Texture* Ptr, const SDL_Rect& Aabb);
	void UpdateAndRender(SDL_Renderer*);
};

Anim_System::Anim_System()
{
	for(int i = 0; i < s_MaxAnims; i++)
	{
		FadeOuts[i].Graphic = {0};
		FadeOuts[i].Alpha = 0.0f;
	}
}

void
Anim_System::AddFadeOut(SDL_Texture* Ptr, const SDL_Rect& Aabb)
{
	for(int i = 0; i < s_MaxAnims; i++)
	{
		if(FadeOuts[i].Graphic.Ptr == NULL)
		{
			FadeOuts[i].Graphic = {Ptr, Aabb};
			FadeOuts[i].Alpha = 1.0f;
			break;
		}
	}
}

void
Anim_System::UpdateAndRender(SDL_Renderer* Renderer)
{
	for(int i = 0; i < s_MaxAnims; i++)
	{
		if(FadeOuts[i].Graphic.Ptr != NULL)
		{
			if(FadeOuts[i].Alpha > 0)
			{
				SDL_SetTextureAlphaMod(FadeOuts[i].Graphic.Ptr, Lerp(0, 255, FadeOuts[i].Alpha));
				FadeOuts[i].Graphic.Render(Renderer);
				FadeOuts[i].Alpha -= 0.1f;
			}
			else
			{
				FadeOuts[i].Graphic = {0};
				FadeOuts[i].Alpha = 0.0f;
			}
		}
	}
}