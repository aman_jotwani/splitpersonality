@echo off

if not defined DevEnvDir call "buildshell.bat"

mkdir ..\build
pushd ..\build
set INCLUDE=C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Include;%INCLUDE%
set INCLUDE=D:\Third-party tools\vs_dev_lib\SDL2-devel-2.0.3-VC\include;%INCLUDE%
set INCLUDE=D:\Third-party tools\vs_dev_lib\SDL2_image-devel-2.0.0-VC\include;%INCLUDE%
set INCLUDE=D:\Third-party tools\vs_dev_lib\SDL2_ttf-devel-2.0.12-VC\include;%INCLUDE%
set INCLUDE=D:\Third-party tools\vs_dev_lib\SDL2_mixer-devel-2.0.0-VC\SDL2_mixer-2.0.0\include;%INCLUDE%
set LIB=D:\Third-party tools\vs_dev_lib\SDL2-devel-2.0.3-VC\lib\x86;%LIB%
set LIB=D:\Third-party tools\vs_dev_lib\SDL2_mixer-devel-2.0.0-VC\SDL2_mixer-2.0.0\lib\x86;%LIB%
set LIB=D:\Third-party tools\vs_dev_lib\SDL2_ttf-devel-2.0.12-VC\lib\x86;%LIB%
set LIB=D:\Third-party tools\vs_dev_lib\SDL2_image-devel-2.0.0-VC\lib\x86;%LIB%
set LIB=C:\Program Files (x86)\Microsoft DirectX SDK (June 2010)\Lib\x86;%LIB%

cl -W3 -EHsc -FC -Zi ..\SplitPersonalityTheGame\win32_splitgame.cpp ..\SplitPersonalityTheGame\sdl_utils.cpp /link user32.lib gdi32.lib winmm.lib SDL2.lib SDL2main.lib SDL2_image.lib SDL2_mixer.lib SDL2_ttf.lib -SUBSYSTEM:CONSOLE -NODEFAULTLIB:msvcrt.lib
popd