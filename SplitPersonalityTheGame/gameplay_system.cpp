// things to teach:
// 1) how to move
// 2) picking stuff
// 3) what do EnemyManager look like
// 4) how to kill them
// 5) how to switch
// 6) practice the switch for a while

// TODO: 1) Remove all traces of charged arrow. or use it as a pickup.
// 		 2) Fix ui. done. Better tutorial.
//		 3) Testing on all resolutions and Mac support. TODO: need to scale enemy speed according to resolution?
// 		 4) Background and music.
//		 5) Score system. or something to tell the player that they are advancing. even this must be handled with ai.
//		 6) Wave ai.
//		 7) Controller support.
//		 8) jump added. add charging EnemyManager that can only be dodged.
//		 9) Hard and fun Boss Fight.
//		 10) game over prompt. its there with wave ai.

class Gameplay_System
{
	SDL_Renderer*  Renderer;
	SDL_Rect   	   GroundAabb;
	InputState 	   P1Input;
	InputState 	   P2Input;
	Anim_System    Anims;
	Powerup_System WeaponManager;
	Enemy_System   EnemyManager;
	HUD            GameUI;
	bool           TutorialMode;
	uint32_t       TutStage;

public:

	Main_Character Player;
	Ai_System      Director;

	void Init(SDL_Renderer* Renderer);
	void GameInit();
	void HandleEvents(const SDL_Event& Event);
	void HandlePlayerInput();
	void UpdateAndRender();
	void CheckCollisions();
	void RenderUI();
	void TeachGame();
};

void 
Gameplay_System::Init(SDL_Renderer* Renderer)
{
	this->Renderer = Renderer;
	GroundAabb = {0, g_ResolutionHeight - (int)(0.1f*g_ResolutionHeight), g_ResolutionWidth, (int)(0.1f*g_ResolutionHeight)};
	LoadImage(Renderer, "..\\assets\\graphics\\PlayerOne\\slice09_09.png", &Player.PlayerOneRight);
	LoadImage(Renderer, "..\\assets\\graphics\\PlayerTwo\\slice09_09.png", &Player.PlayerTwoRight);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\pig0.png", &EnemyManager.TexArrowEnemy);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\bird0.png", &EnemyManager.TexSwordEnemy);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\bullet.png", &EnemyManager.TexBullet);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\sword_right.png", &WeaponManager.TexSwordRight);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\arrow_right.png", &WeaponManager.TexArrowRight);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\shield.png", &WeaponManager.TexShieldRight);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\hp.png", &WeaponManager.TexHp);

	P1Input = {false, false, false, false};
	P2Input = {false, false, false, false};

	Player.Init();
	WeaponManager.Init();
	EnemyManager.Init(g_ResolutionWidth, g_ResolutionHeight, GroundAabb.h);
	GameUI.Init(Renderer);

	TutorialMode = true;
	TutStage = 0;
	Player.ArrowsHeld = 0;
	WeaponManager.SpawnArrowOnGround(0.6f*g_ResolutionWidth, 
			 						 GroundAabb.y - WeaponManager.TexArrowRight.DrawRect.h);
	WeaponManager.SpawnArrowOnGround(0.6f*g_ResolutionWidth + WeaponManager.TexArrowRight.DrawRect.w, 
		                             GroundAabb.y - WeaponManager.TexArrowRight.DrawRect.h);
	// WeaponManager.SpawnArrowOnGround(0.6f*g_ResolutionWidth + 2*WeaponManager.TexArrowRight.DrawRect.w, 
	// 								 GroundAabb.y - WeaponManager.TexArrowRight.DrawRect.h);
	// WeaponManager.SpawnArrowOnGround(0.6f*g_ResolutionWidth + 3*WeaponManager.TexArrowRight.DrawRect.w, 
	// 								 GroundAabb.y - WeaponManager.TexArrowRight.DrawRect.h);
	// WeaponManager.SpawnArrowOnGround(0.6f*g_ResolutionWidth + 4*WeaponManager.TexArrowRight.DrawRect.w, 
	// 								 GroundAabb.y - WeaponManager.TexArrowRight.DrawRect.h);

	EnemyManager.SpawnStdEnemyGrounded(0.8f*g_ResolutionWidth, false);
	EnemyManager.SpawnStdEnemyGrounded(0.2f*g_ResolutionWidth, true);
}

void 
Gameplay_System::GameInit()
{
	P1Input = {false, false, false, false};
	P2Input = {false, false, false, false};

	Director.Init();
	Player.Init();
	WeaponManager.Init();
	EnemyManager.Init(g_ResolutionWidth, g_ResolutionHeight, GroundAabb.h);
	GameUI.Init(Renderer);
}

void
Gameplay_System::HandleEvents(const SDL_Event& Event)
{
	if(Event.type == SDL_KEYDOWN)
	{
		if(Event.key.keysym.sym == SDLK_k && Event.key.repeat == 0)
		{
			P1Input.ActionPressed = true;
		}

		if(Event.key.keysym.sym == SDLK_o && Event.key.repeat == 0)
		{
			P1Input.Action2Pressed = true;
		}

		if(Event.key.keysym.sym == SDLK_s && Event.key.repeat == 0)
		{
			P2Input.ActionPressed = true;
		}

		if(Event.key.keysym.sym == SDLK_q)
		{
			P2Input.Action2Pressed = true;
		}

		if(Event.key.keysym.sym == SDLK_i && Event.key.repeat == 0)
		{
			P1Input.SwitchPressed = true;
		}
		if(Event.key.keysym.sym == SDLK_w && Event.key.repeat == 0)
		{
			P2Input.SwitchPressed = true;
		}

		switch(Event.key.keysym.sym)
		{
			case SDLK_t:
			{
				if(TutorialMode)
				{
					TutorialMode = false;
					GameInit();					
				}
			}
			break;

			case SDLK_l:
			{
				P1Input.RightPressed = true;
			}
			break;

			case SDLK_j:
			{
				P1Input.LeftPressed = true;
			}
			break;

			case SDLK_d:
			{
				P2Input.RightPressed = true;
			}
			break;

			case SDLK_a:
			{
				P2Input.LeftPressed = true;
			}
			break;
		}
	}
	else if(Event.type == SDL_KEYUP)
	{
		switch(Event.key.keysym.sym)
		{
			case SDLK_l:
			{
				P1Input.RightPressed = false;
			}
			break;

			case SDLK_j:
			{
				P1Input.LeftPressed = false;
			}
			break;

			case SDLK_d:
			{
				P2Input.RightPressed = false;
			}
			break;

			case SDLK_a:
			{
				P2Input.LeftPressed = false;
			}
			break;

			case SDLK_o:
			{
				P1Input.Action2Pressed = false;
			}
			break;

			case SDLK_q:
			{
				P2Input.Action2Pressed = false;
			}
			break;

			case SDLK_i:
			{
				P1Input.SwitchPressed = false;
			}
			break;

			case SDLK_w:
			{
				P2Input.SwitchPressed = false;
			}
			break;
		}
	}
}

void
Gameplay_System::HandlePlayerInput()
{
	Player.State.Velocity.x = 0.0f;
	if(Player.IsP1Right)
	{
		if(P1Input.RightPressed /*&& !P2Input.RightPressed*/)
		{
			Player.MoveRight();
		}

		if(/*!P1Input.LeftPressed &&*/ P2Input.LeftPressed)
		{
			Player.MoveLeft();
		}
	}
	else
	{
		if(/*!P1Input.RightPressed &&*/ P2Input.RightPressed)
		{
			Player.MoveRight();
		}

		if(P1Input.LeftPressed /*&& !P2Input.LeftPressed*/)
		{
			Player.MoveLeft();
		}		
	}

	if(P1Input.ActionPressed)
	{
		float spawn_y = (Player.Aabb.y + Player.PlayerOneRight.DrawRect.h/2)*g_PhyScale;
		// if(P1Input.Action2Pressed && 
		//   (GetMSElapsedSince(WeaponManager.ChargedArrowCounter) > Powerup_System::s_ChargeDurationMS))
		// {
		// 	WeaponManager.ShootChargedArrow(&Player.ArrowsHeld, Player.Aabb.x, spawn_y, Player.Aabb.w, Player.IsP1Right);
		// 	P1Input.Action2Pressed = false;
		// }
		// else
		// {
			WeaponManager.ShootArrow(&Player.ArrowsHeld, Player.Aabb.x, spawn_y, Player.Aabb.w, Player.IsP1Right);
		// }
		P1Input.ActionPressed = false;
	}

	if(!P2Input.Action2Pressed && WeaponManager.TheShield.InUse)
	{
		WeaponManager.TheShield.InUse = false;
	}
	else if(P2Input.Action2Pressed)
	{
		if(WeaponManager.TheSword.InUse)
		{
			// remove the sword, add the shield, set both actions to false
			WeaponManager.TheSword.InUse = false;
			P2Input.ActionPressed = false;
		}
		WeaponManager.TheShield.InUse = true;
	}

	if(P2Input.ActionPressed && !WeaponManager.TheSword.InUse)
	{
		if(WeaponManager.TheShield.InUse)
		{
			// remove the shield, switch on the sword, set both actions to false
			WeaponManager.TheShield.InUse = false;
			P2Input.Action2Pressed = false;
		}
		WeaponManager.TheSword.InUse = true;
		P2Input.ActionPressed = false;
		WeaponManager.SwordExpiryCounter = SDL_GetTicks();			
	}

	if(P1Input.SwitchPressed && P2Input.SwitchPressed)
	{
		Player.SwitchSides();
		P1Input.SwitchPressed = false;
		P2Input.SwitchPressed = false;
	}
}

void
Gameplay_System::CheckCollisions()
{
	for(uint32_t ArrowIndex = 0; ArrowIndex < WeaponManager.Arrows.Size(); ArrowIndex++)
	{
		Arrow* CurrArrow = WeaponManager.Arrows.GetIfActive(ArrowIndex);

		if(CurrArrow && CurrArrow->Flag == ArrowShot)
		{
			// can hit and kill one stdmovtspeed enemy, becomes inactive
			for(uint32_t EnemyIndex = 0; EnemyIndex < EnemyManager.Enemies.Size(); EnemyIndex++)
			{
				Enemy* active_enemy = EnemyManager.Enemies.GetIfActive(EnemyIndex);
				if(active_enemy && active_enemy->EnemyType == ArrowKillable &&
				   Collides(&CurrArrow->Aabb, &active_enemy->Aabb))
			   {
					// arrow--arrow-killable handling
			   		EnemyManager.Enemies.SetInactive(EnemyIndex);
			   		WeaponManager.Arrows.SetInactive(ArrowIndex);
					Anims.AddFadeOut(EnemyManager.TexArrowEnemy.Ptr, active_enemy->Aabb);
					Director.CurrParams.EnemiesKilled += 1;
					Player.PointsEarned += 10;
					if(RandomDecision() && RandomDecision())
					{
						WeaponManager.SpawnHp(active_enemy->Aabb.x, active_enemy->Aabb.y);
					}
					break; // to ensure a "used" arrow doesnt hit two enemies.
				}
			}

			if(EnemyManager.Goomba.IsAlive)
			{
				// TODO: handle charging goomba case
				if(std::abs(EnemyManager.Goomba.State.Velocity.x) <= (Enemy_System::s_StdMovtSpeed - 3.0f) &&
				   Collides(&CurrArrow->Aabb, &EnemyManager.Goomba.Aabb))
				{
					EnemyManager.Goomba.ArrowsReqd -= 1;
					WeaponManager.Arrows.SetInactive(ArrowIndex);
					if(EnemyManager.Goomba.ArrowsReqd == 0)
					{
						EnemyManager.Goomba.IsAlive = false;
						Anims.AddFadeOut(EnemyManager.TexArrowEnemy.Ptr, EnemyManager.Goomba.Aabb);
						Director.CurrParams.EnemiesKilled += 1;
						Player.PointsEarned += 50;
					}
				}
			}
		}
	}

	for(uint32_t EnemyIndex = 0; EnemyIndex < EnemyManager.Enemies.Size(); EnemyIndex++)
	{
		Enemy* active_enemy = EnemyManager.Enemies.GetIfActive(EnemyIndex);
		// enemy collision with player
		if(active_enemy)
		{
			EnemyState type = active_enemy->EnemyType;

			if(!(active_enemy->DamageDealt) && Collides(&active_enemy->Aabb, &Player.Aabb))
			{
				if(type == ArrowKillable)
				{
					Player.HealthPoints -= 10;
				}
				else if(type == SwordKillable)
				{
					Player.HealthPoints -= 20;
				}
				else if(type == Charging)
				{
					Player.HealthPoints -= 30;
				}
				active_enemy->DamageDealt = true;
			}

			if(WeaponManager.TheSword.InUse && type == SwordKillable &&
			   Collides(&active_enemy->Aabb, &WeaponManager.TheSword.Aabb))
			{
				// sword -- sword-killable handling
				EnemyManager.Enemies.SetInactive(EnemyIndex);
				if(RandomDecision())
				{
					WeaponManager.PopArrowFromEnemy(active_enemy->Aabb.x, active_enemy->Aabb.y);
				}
				Anims.AddFadeOut(EnemyManager.TexSwordEnemy.Ptr, active_enemy->Aabb);
				Player.PointsEarned += 20;
				Director.CurrParams.EnemiesKilled += 1;
			}
		} 
	}

	for(uint32_t ArrowIndex = 0; ArrowIndex < WeaponManager.Arrows.Size(); ArrowIndex++)
	{
		Arrow* CurrArrow = WeaponManager.Arrows.GetIfActive(ArrowIndex);
		if(CurrArrow)
		{
			// arrow collision with ground
			if(CurrArrow->Flag != ArrowGrounded && 
			   Collides(&CurrArrow->Aabb, &GroundAabb))
			{
				CurrArrow->Flag = ArrowGrounded;
				CurrArrow->State.Grounded = true;
				CurrArrow->AccFuncPtr = NoAcceleration;
				CurrArrow->State.Velocity = {0.0f, 0.0f};
				CurrArrow->State.Position.y = (GroundAabb.y - CurrArrow->Aabb.h)*g_PhyScale;
				UpdateAabb(CurrArrow->State.Position, &CurrArrow->Aabb);
			}

			if(CurrArrow->Flag == ArrowGrounded && 
			   Collides(&CurrArrow->Aabb, &Player.Aabb))
			{
				Player.ArrowsHeld += 1;
				WeaponManager.Arrows.SetInactive(ArrowIndex);
			}
		}
	}

	for(int BulletIndex = 0; BulletIndex < Enemy_System::s_MaxBullets; BulletIndex++)
	{
		if(EnemyManager.BulletPool[BulletIndex].IsActive)
		{
			if(WeaponManager.TheShield.InUse && 
			   Collides(&WeaponManager.TheShield.Aabb, &EnemyManager.BulletPool[BulletIndex].Aabb))
			{
				EnemyManager.BulletPool[BulletIndex].IsActive = false;
			}
			else if(Collides(&Player.Aabb, &EnemyManager.BulletPool[BulletIndex].Aabb))
			{
				Player.HealthPoints -= 10;
				EnemyManager.BulletPool[BulletIndex].IsActive = false;
			}
		}
	}

	// hp pickup ground collision
	for(size_t i = 0; i < WeaponManager.HpDrops.Size(); i++)
	{
		HpPickup* CurrHpDrop = WeaponManager.HpDrops.GetIfActive(i);

		if(CurrHpDrop)
		{
			if(!(CurrHpDrop->State.Grounded) && 
		       Collides(&CurrHpDrop->Aabb, &GroundAabb))
			{
				CurrHpDrop->State.Grounded = true;
				CurrHpDrop->State.Position.x = CurrHpDrop->State.Position.x;
				CurrHpDrop->State.Position.y = (GroundAabb.y - WeaponManager.TexHp.DrawRect.h)*g_PhyScale;
				CurrHpDrop->State.Velocity = {0.0f, 0.0f};
				UpdateAabb(CurrHpDrop->State.Position, &(CurrHpDrop->Aabb));
			}

			if(Collides(&CurrHpDrop->Aabb, &Player.Aabb))
			{
				if(Player.HealthPoints < 100)
				{
					Player.HealthPoints += CurrHpDrop->Amount;
				}
				WeaponManager.HpDrops.SetInactive(i);
			}			
		}
	}

	// player ground collision
	if(Collides(&(Player.Aabb), &(GroundAabb)))
	{
		Player.State.Grounded = true;
		Player.State.Velocity.y = 0.0f;
		Player.State.Position.y = (GroundAabb.y - Player.PlayerTwoRight.DrawRect.h)*g_PhyScale;
		UpdateAabb(Player.State.Position, &Player.Aabb);
	}
}

void
Gameplay_System::UpdateAndRender()
{	
	if(!TutorialMode)
	{
		GameUI.ShowSwitchStatus(Renderer, P1Input.SwitchPressed, P2Input.SwitchPressed);
		Director.Think(Player.ArrowsHeld, &EnemyManager, &WeaponManager);
	}
	else
	{
		TeachGame();
	}

	bool past = Player.IsP1Right;
	HandlePlayerInput();
	bool curr = Player.IsP1Right;
	if(past != curr)
	{
		GameUI.Narrator.DrawRect.x = GetRandomInt(50, 0.6f*g_ResolutionWidth);
		GameUI.Narrator.DrawRect.y = GetRandomInt(50, 0.5f*g_ResolutionHeight);
		Anims.AddFadeOut(GameUI.Narrator.Ptr, GameUI.Narrator.DrawRect);
	}

	Player.UpdateAndRender(Renderer);
	
	WeaponManager.UpdateAndRender(Renderer, g_ResolutionWidth, Player.Aabb.x, Player.Aabb.y, Player.Aabb.w, Player.IsP1Right);
	
	EnemyManager.UpdateAndRender(Renderer);
	
	Anims.UpdateAndRender(Renderer);
	
	CheckCollisions();

	if(Player.HealthPoints > 100)
	{
		Player.HealthPoints = 100;
	}
	
	RenderUI();

	// drawing ground
	SDL_SetRenderDrawColor(Renderer, 0x22, 0x32, 0x00, 0xFF);
	SDL_RenderFillRect(Renderer, &GroundAabb);
	SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0x00, 0xFF);
}

void
Gameplay_System::TeachGame()
{
	uint32_t total_enemies = EnemyManager.GetNumArrowKillables() + EnemyManager.GetNumSwordKillables();
	if(TutStage == 0 && !Player.IsP1Right)
	{
		TutStage = 1;
	}
	else if(TutStage == 1 && Player.ArrowsHeld > 0)
	{
		TutStage = 2;
	}
	else if(TutStage == 2 && total_enemies == 0)
	{
		EnemyManager.SpawnEnemy(false, ArrowKillable);
		EnemyManager.SpawnEnemy(true, SwordKillable);
		TutStage = 3;
	}
	else if(TutStage == 3 && total_enemies == 0 && Player.HealthPoints < 100)
	{
		Player.HealthPoints = 100;
		EnemyManager.SpawnEnemy(false, ArrowKillable);
		EnemyManager.SpawnEnemy(true, SwordKillable);
	}
	else if(TutStage == 3 && total_enemies == 0 && Player.HealthPoints == 100)
	{
		TutorialMode = false;
		TutStage = 0;
		GameInit();
	}
}

void
Gameplay_System::RenderUI()
{
	GameUI.TextArrows.Render(Renderer);
	GameUI.NumArrows.Render(Renderer, Player.ArrowsHeld);
	GameUI.TextHP.Render(Renderer);
	GameUI.NumHP.Render(Renderer, Player.HealthPoints);
	GameUI.ShowControls(Renderer, TutStage, TutorialMode, Player.IsP1Right);

	// GameUI.TextCharge.Render(Renderer);

	// int multiple = 15; 		
	// int padding = 10;
	// int bar_width = GameUI.BarLeftBoundary.DrawRect.w + multiple*GameUI.BarMid.DrawRect.w + GameUI.BarRightBoundary.DrawRect.w;
	// SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0xFF, 0xFF);
	// SDL_Rect Boundary = {GameUI.TextCharge.DrawRect.x + GameUI.TextCharge.DrawRect.w - 2, GameUI.TextCharge.DrawRect.y - 2, 
	// 					 bar_width + 2, GameUI.BarMid.DrawRect.h + 2};
 //    SDL_RenderDrawRect(Renderer, &Boundary);
 //    SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0x00, 0xFF);

	// if(P1Input.Action2Pressed)
	// {
	// 	Uint32 ms_left = Powerup_System::s_ChargeDurationMS - GetMSElapsedSince(WeaponManager.ChargedArrowCounter);
	// 	int width = (int)(ms_left * ((float)bar_width / Powerup_System::s_ChargeDurationMS));
	// 	if(width > 0 && width <= bar_width)
	// 	{
	// 		GameUI.BarLeftBoundary.Render(Renderer, GameUI.TextCharge.DrawRect.x + GameUI.TextCharge.DrawRect.w, 
	// 									  GameUI.TextCharge.DrawRect.y);
	// 		int filler = 0;
	// 		int bar_x = GameUI.BarLeftBoundary.DrawRect.x + GameUI.BarLeftBoundary.DrawRect.w;
	// 		int bar_y = GameUI.BarLeftBoundary.DrawRect.y;
	// 		while(filler < (width - GameUI.BarLeftBoundary.DrawRect.w - GameUI.BarRightBoundary.DrawRect.w))
	// 		{
	// 			GameUI.BarMid.Render(Renderer, bar_x, bar_y);
	// 			filler += GameUI.BarMid.DrawRect.w;
	// 			bar_x += GameUI.BarMid.DrawRect.w;
	// 		}
	// 		GameUI.BarRightBoundary.Render(Renderer, bar_x, bar_y);
	// 	}
	// }

	// weapon type on top of enemy
	for(uint32_t EnemyIndex = 0; EnemyIndex < EnemyManager.Enemies.Size(); ++EnemyIndex)
	{
		Enemy* active_enemy = EnemyManager.Enemies.GetIfActive(EnemyIndex);
		if(active_enemy)
		{
			switch(active_enemy->EnemyType)
			{
				case ArrowKillable:
				{
					WeaponManager.TexArrowRight.Render(Renderer, active_enemy->Aabb.x,
									   				   active_enemy->Aabb.y - WeaponManager.TexArrowRight.DrawRect.h);
				}
				break;

				case SwordKillable:
				{
					WeaponManager.TexSwordRight.Render(Renderer, active_enemy->Aabb.x,
									   				   active_enemy->Aabb.y - WeaponManager.TexSwordRight.DrawRect.h);
				}
				break;

				case Charging:
				{
					// cant be killed by weapons
				}
				break;
			}			
		}
	}

	if(EnemyManager.Goomba.IsAlive)
	{
		int y = EnemyManager.Goomba.Aabb.y - WeaponManager.TexArrowRight.DrawRect.h;
		for(uint32_t i = 0; i < EnemyManager.Goomba.ArrowsReqd; i++)
		{
			WeaponManager.TexArrowRight.Render(Renderer,
											   EnemyManager.Goomba.Aabb.x, y);
			y -= WeaponManager.TexArrowRight.DrawRect.h;
		}
	}
}