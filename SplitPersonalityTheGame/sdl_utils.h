#include <SDL.h>
#undef main
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#include <stdio.h>

struct PlatformData
{
    SDL_Window* Window;
    SDL_Renderer* Renderer;
    uint32_t ScreenWidth;
    uint32_t ScreenHeight;
    char* WindowTitle;
};

struct Texture
{
    SDL_Texture* Ptr;
    SDL_Rect DrawRect;

    void Render(SDL_Renderer* Renderer);
    void Render(SDL_Renderer* Renderer, int DrawX, int DrawY);
    void RenderFlipped(SDL_Renderer* Renderer, int DrawX, int DrawY);
};

struct NumTexture
{
    Texture   Tex;
    TTF_Font* Font;
    SDL_Color NumColor;
    int       Num;

    NumTexture();
    void Init(char* FontFilename, int Num, int Height, int DrawX, int DrawY, SDL_Color Color);
    void Render(SDL_Renderer* Renderer, int DrawX, int DrawY, int NewNum);
    void Render(SDL_Renderer* Renderer, int NewNum);
    void Render(SDL_Renderer* Renderer);
};

int InitSDL(PlatformData* Engine, char* Title, uint32_t ScreenWidth, uint32_t ScreenHeight);

void LoadImage(SDL_Renderer* Renderer, char* Filename, Texture* Tex);

void LoadImageWhiteColorKey(SDL_Renderer* Renderer, char* Filename, Texture* Tex);

void LoadImageColorKey(SDL_Renderer* Renderer, char* Filename, Texture* Tex, uint8_t Red, uint8_t Green, uint8_t Blue);

void Load_SolidText(SDL_Renderer* Renderer, char* Text, TTF_Font* Font, SDL_Color Color, Texture* Tex);

inline bool
Collides(SDL_Rect* A, SDL_Rect* B)
{
    if(SDL_HasIntersection(A, B) == SDL_TRUE)
    {
        return (true);
    }
    else
    {        
        return (false);
    }
}