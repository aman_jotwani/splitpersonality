#include "sdl_utils.h"

////////////////////// Texture function definitions ///////////////////////

void
Texture::Render(SDL_Renderer* Renderer, int DrawX, int DrawY)
{
    DrawRect.x = DrawX;
    DrawRect.y = DrawY;
    SDL_RenderCopy(Renderer, Ptr, NULL, &DrawRect);
}

void
Texture::Render(SDL_Renderer* Renderer)
{
    SDL_RenderCopy(Renderer, Ptr, NULL, &DrawRect);
}

void
Texture::RenderFlipped(SDL_Renderer* Renderer, int DrawX, int DrawY)
{
    DrawRect.x = DrawX;
    DrawRect.y = DrawY;
    SDL_RenderCopyEx(Renderer, Ptr, NULL, &DrawRect, 0.0f, NULL, SDL_FLIP_HORIZONTAL);
}

////////////////////// NumTexture function definitions //////////////////

NumTexture::NumTexture()
{
    Tex = {NULL, {0}};
    Font = NULL;
    NumColor = {0};
    Num = 0;
}

void
NumTexture::Init(char* FontFilename, int NewNum, int Height, int DrawX, int DrawY, SDL_Color Color)
{
    Num = NewNum;
    Font = TTF_OpenFont(FontFilename, Height);
    NumColor = Color;
    Tex.DrawRect.x = DrawX;
    Tex.DrawRect.y = DrawY;
    // README: this is set because it helps ui positioning. Tex cant be loaded here because caller cannot
    // provide SDL_Renderer*
    Tex.DrawRect.h = Height;
}

void
NumTexture::Render(SDL_Renderer* Renderer)
{
    Tex.Render(Renderer);
}

void
NumTexture::Render(SDL_Renderer* Renderer, int DrawX, int DrawY, int NewNum)
{
    if(NewNum != Num)
    {
        Num = NewNum;
        char Buffer[4];
        Buffer[3] = '\0';
        _snprintf_s(Buffer, 3, "%d", NewNum);
        Load_SolidText(Renderer, Buffer, Font, NumColor, &Tex);
    }
    Tex.DrawRect.x = DrawX;
    Tex.DrawRect.y = DrawY;
    Tex.Render(Renderer);
}

void
NumTexture::Render(SDL_Renderer* Renderer, int NewNum)
{
    if(NewNum != Num)
    {
        Num = NewNum;
        char Buffer[4];
        Buffer[3] = '\0';
        _snprintf_s(Buffer, 3, "%d", NewNum);
        Load_SolidText(Renderer, Buffer, Font, NumColor, &Tex);        
    }
    Tex.Render(Renderer);
}

////////////////////// commonly used functions ////////////////////////

int 
InitSDL(PlatformData* Engine, char* Title, uint32_t ScreenWidth, uint32_t ScreenHeight)
{
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        Engine->WindowTitle = Title;
        Engine->ScreenWidth = ScreenWidth;
        Engine->ScreenHeight = ScreenHeight;

        Engine->Window = SDL_CreateWindow(Engine->WindowTitle, 
                                          SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                          Engine->ScreenWidth, Engine->ScreenHeight,
                                          SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        if(Engine->Window)
        {
            Engine->Renderer = SDL_CreateRenderer(Engine->Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            if(Engine->Renderer)
            {
                if(IMG_Init(IMG_INIT_PNG) == (IMG_INIT_PNG))
                {
                    if(TTF_Init() == -1)
                    {
                        SDL_Log("font lib init failed.");                    
                    }

                    if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,
                                     2, 1024) == -1)
                    {
                        SDL_Log("audio lib init failed. Mix_OpenAudio failed.");                 
                    }
                    return 1;
                }
                else
                {
                    SDL_Log("image system didnt initialize.");
                    return 0;
                }
            }
            else
            {
                SDL_Log("renderer creation failed.");
                return 0;
            }
        }
        else
        {
            SDL_Log("window creation failed.");
            return 0;
        }
    }
    else
    {
        SDL_Log("SDL init failed.");
        return(0);
    }
}

void LoadImage(SDL_Renderer* Renderer, char* Filename, Texture* Tex)
{
    SDL_Surface* Surf = IMG_Load(Filename);
    if(Surf != NULL)
    {
        Tex->Ptr = SDL_CreateTextureFromSurface(Renderer, Surf);
        if(Tex->Ptr != NULL)
        {
        	Tex->DrawRect.x = 0;
        	Tex->DrawRect.y = 0;
            Tex->DrawRect.w = Surf->w;
            Tex->DrawRect.h = Surf->h;
        }
        else
            SDL_Log("texture from surface creation failed for %s.\n", Filename);

        SDL_FreeSurface(Surf);
    }
    else
        SDL_Log("IMG_Load failed to load %s.\n", Filename);
}

void LoadImageWhiteColorKey(SDL_Renderer* Renderer, char* Filename, Texture* Tex)
{
    SDL_Surface* Surf = IMG_Load(Filename);
    if(Surf != NULL)
    {
        if(SDL_SetColorKey(Surf, SDL_TRUE, SDL_MapRGB(Surf->format, 0xFF, 0xFF, 0xFF)) == 0)
        {
            Tex->Ptr = SDL_CreateTextureFromSurface(Renderer, Surf);
            if(Tex->Ptr != NULL)
            {
                Tex->DrawRect.x = 0;
                Tex->DrawRect.y = 0;
                Tex->DrawRect.w = Surf->w;
                Tex->DrawRect.h = Surf->h;
                SDL_FreeSurface(Surf);
            }
            else
                SDL_Log("texture from surface creation failed for %s.\nSDL_GetError: %s.\n", Filename, SDL_GetError());         
        }
        else
        {
            SDL_Log("Color keying failed in LoadImageWhiteColorKey for %s.\n", Filename);
        }
    }
    else
        SDL_Log("IMG_Load failed to load %s.\n", Filename);
}

void LoadImageColorKey(SDL_Renderer* Renderer, char* Filename, Texture* Tex, uint8_t Red, uint8_t Green, uint8_t Blue)
{
    SDL_Surface* Surf = IMG_Load(Filename);
    if(Surf != NULL)
    {
        if(SDL_SetColorKey(Surf, SDL_TRUE, SDL_MapRGB(Surf->format, Red, Green, Blue)) == 0)
        {
            Tex->Ptr = SDL_CreateTextureFromSurface(Renderer, Surf);
            if(Tex->Ptr != NULL)
            {
                Tex->DrawRect.x = 0;
                Tex->DrawRect.y = 0;
                Tex->DrawRect.w = Surf->w;
                Tex->DrawRect.h = Surf->h;
                SDL_FreeSurface(Surf);
            }
            else
                SDL_Log("texture from surface creation failed for %s. System error- %s.\n", Filename, SDL_GetError());         
        }
        else
        {
            SDL_Log("Color keying failed in LoadImageWhiteColorKey for %s. System error- %s.\n", Filename, SDL_GetError());
        }
    }
    else
        SDL_Log("IMG_Load failed to load %s. System error-%s.\n", Filename, SDL_GetError());
}

void Load_SolidText(SDL_Renderer* Renderer, char* Text, TTF_Font* Font, SDL_Color Color, Texture* Tex)
{
    SDL_Surface* temp = TTF_RenderText_Solid(Font, Text, Color);
    if(temp)
    {
        Tex->Ptr = SDL_CreateTextureFromSurface(Renderer, temp);
        if(Tex->Ptr != NULL)
        {
            Tex->DrawRect.w = temp->w; 
            Tex->DrawRect.h = temp->h;
            SDL_FreeSurface(temp);
        }
        else
            SDL_Log("texture from surface creation failed for this text - %s. System error- %s.\n", Text, SDL_GetError());
    }
    else
        SDL_Log("TTF_RenderText_Solid failed for %s. System error- %s.\n", Text, SDL_GetError());
}