const float Main_Character::sMovtSpeed = 6.0f;
const float Main_Character::sJumpSpeed = -20.0f;

void
Main_Character::Init()
{
	State.Position = {(g_ResolutionWidth/2) * g_PhyScale, (g_ResolutionHeight - 0.1f*g_ResolutionHeight - PlayerTwoRight.DrawRect.h)*g_PhyScale};
	State.Velocity = {0.0f, 0.0f};
	State.Grounded = true;
	Aabb = {static_cast<int>(State.Position.x / g_PhyScale),
		 	static_cast<int>(State.Position.y / g_PhyScale),
			PlayerOneRight.DrawRect.w + PlayerTwoRight.DrawRect.w, PlayerTwoRight.DrawRect.h};

	IsP1Right = true;
	ArrowsHeld = 10;
	PointsEarned = 0;
	HealthPoints = 100;
}

void
Main_Character::MoveRight()
{
	State.Velocity.x = sMovtSpeed;
}

void
Main_Character::MoveLeft()
{
	State.Velocity.x = -sMovtSpeed;
}

void
Main_Character::SwitchSides()
{
	IsP1Right = IsP1Right ? false : true;
}

void
Main_Character::Jump()
{
	if(State.Grounded)
	{
		State.Velocity.y = sJumpSpeed;
		State.Grounded = false;		
	}
}

void
Main_Character::UpdateAndRender(SDL_Renderer* Renderer)
{
	vec2 (*AccFunc)(const PhysicsData&);
	
	if(State.Grounded)
	{
		AccFunc = NoAcceleration;
	}
	else
	{
		AccFunc = StandardGravity;
	}

	Physics_System::Integrate(&State, AccFunc);
	UpdateAabb(State.Position, &Aabb);

	if(Aabb.x < 0)
	{
		Aabb.x = 0;
		State.Position.x = Aabb.x * g_PhyScale;
	}
	else if(Aabb.x > (g_ResolutionWidth - Aabb.w))
	{
		Aabb.x = (g_ResolutionWidth - Aabb.w);
		State.Position.x = Aabb.x * g_PhyScale;
	}

	if(IsP1Right)
	{
		PlayerOneRight.Render(Renderer, Aabb.x + PlayerTwoRight.DrawRect.w, Aabb.y + PlayerTwoRight.DrawRect.h - PlayerOneRight.DrawRect.h);
		PlayerTwoRight.RenderFlipped(Renderer, Aabb.x, Aabb.y);
	}
	else
	{
		PlayerOneRight.RenderFlipped(Renderer, Aabb.x, Aabb.y + PlayerTwoRight.DrawRect.h - PlayerOneRight.DrawRect.h);
		PlayerTwoRight.Render(Renderer, Aabb.x + PlayerOneRight.DrawRect.w, Aabb.y);
	}
}