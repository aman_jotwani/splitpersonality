// windows-dependent utility functions
// inline LARGE_INTEGER
// Win32GetPerformanceCounter()
// {
//     LARGE_INTEGER Result;
//     QueryPerformanceCounter(&Result);
//     return Result;
// }

// inline float
// Win32GetMSElapsedSince(LARGE_INTEGER Start)
// {
//     LARGE_INTEGER End = Win32GetPerformanceCounter();
//     float Diff = (float) End.QuadPart - (float) Start.QuadPart;
//     float Result = Diff * g_HardwareTickTime * 1000;
//     return Result;
// }

float Lerp(float Start, float End, float BlendFactor)
{
    if(BlendFactor > 1.0f)
    {
        return End;
    }
    else if(BlendFactor < 0.0f)
    {
        return Start;
    }
    else
    {
        return (Start + BlendFactor*(End - Start));
    }
}

inline Uint32 
GetMSElapsedSince(Uint32 Start)
{
    Uint32 End = SDL_GetTicks();
    return (End-Start);
}

inline float
GetRandomNum(float Min, float Max)
{
    uint32_t RandomNum;
    rand_s(&RandomNum);
    float Result = ((float) RandomNum / ((float) UINT_MAX + 1) * Max) + Min;
    return Result;
}

inline uint32_t
GetRandomInt(uint32_t Min, uint32_t Max)
{
    uint32_t RandomNum;
    rand_s(&RandomNum);
    uint32_t Result = (uint32_t) ((double) RandomNum / ((double) UINT_MAX + 1) * Max) + Min;
    return Result;
}

inline bool
RandomDecision()
{
    uint32_t RandomNum;
    rand_s(&RandomNum);
    if(RandomNum <= UINT_MAX/2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

#define PRINT_BOOL(x) x ? "true" : "false"

inline bool
IS_ON(uint32_t Bitmask, uint32_t Condition)
{
    return Bitmask & Condition ? true : false;
}

inline void
UpdateAabb(const vec2& PhyCoords, SDL_Rect* Aabb)
{
    Aabb->x = (int) (PhyCoords.x / g_PhyScale);
    Aabb->y = (int) (PhyCoords.y / g_PhyScale);
}