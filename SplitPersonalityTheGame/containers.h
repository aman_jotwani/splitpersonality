#include <stdint.h>

struct vec2
{
    float x, y;

    vec2 operator+(const vec2& other) const;
    vec2 operator-(const vec2& other) const;
    vec2 operator*(float scalar) const;
    void operator*=(float scalar);
    void operator+=(const vec2& other);
};

inline vec2
vec2::operator+(const vec2& other) const
{
    vec2 result = {x, y};
    result.x += other.x;
    result.y += other.y;
    return result;
}

inline vec2
vec2::operator-(const vec2& other) const
{
    vec2 result = {x, y};
    result.x -= other.x;
    result.y -= other.y;
    return result;
}

inline vec2
vec2::operator *(float scalar) const
{
    vec2 result = {x, y};
    result.x *= scalar;
    result.y *= scalar;
    return result;
}

inline void
vec2::operator *=(float scalar)
{
    x *= scalar;
    y *= scalar;
}

inline void
vec2::operator +=(const vec2& other)
{
    x += other.x;
    y += other.y;
}

// FIXME: this template requires that Type have an IsActive bool
// this is the reason it wasnt used for enemies, enemies have IsAlive
// which can be changed and probably should be.
// this wasnt used for arrows because arrows have ArrowState instead of IsActive
// NOTE: research iterators! they definitely dont look very pretty though.
// TODO: use templates more effectively. 
template <class Type, uint32_t PoolSize>
class Pool
{
    Type Data[PoolSize];
    bool ActiveList[PoolSize];
public:
    Pool();
    uint32_t Size() const
    {
        return(PoolSize);
    };
    void Init(const Type& Obj);
    void Insert(const Type& Obj);
    void SetInactive(int Index);
    void SetActive(int Index);
    Type* GetIfActive(int Index);
};

template<class Type, uint32_t PoolSize>
Pool<Type, PoolSize>::Pool()
{
    for(size_t i = 0; i < Size(); i++)
    {
        Data[i] = {0};
        ActiveList[i] = false;
    }
}

template <class Type, uint32_t PoolSize>
void Pool<Type, PoolSize>::Init(const Type& Obj)
{
    for(size_t i = 0; i < Size(); ++i)
    {
        Data[i] = Obj;
        ActiveList[i] = false;
    }
}

template <class Type, uint32_t PoolSize>
void Pool<Type, PoolSize>::Insert(const Type& Obj)
{
    bool PoolOverflow = true;
    for(size_t i = 0; i < Size(); i++)
    {
        if(!ActiveList[i])
        {
            Data[i] = Obj;
            ActiveList[i] = true;
            PoolOverflow = false;
            break;
        }
    }
    if(PoolOverflow)
    {
        // deliberately crashing to understand pool sizes
        *(int*)0 = 0;
    }
}

template <class Type, uint32_t PoolSize>
Type* Pool<Type, PoolSize>::GetIfActive(int Index)
{
    if(ActiveList[Index])
    {
        return(&(Data[Index]));
    }
    else
    {
        return(NULL);
    }
}

template <class Type, uint32_t PoolSize>
void Pool<Type, PoolSize>::SetActive(int Index)
{
    ActiveList[Index] = true;
}

template <class Type, uint32_t PoolSize>
void Pool<Type, PoolSize>::SetInactive(int Index)
{
    ActiveList[Index] = false;
}