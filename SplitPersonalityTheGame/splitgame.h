struct PhysicsData 
{
	vec2 Position;
	vec2 Velocity;
	bool Grounded;
};

inline vec2
NoAcceleration(const PhysicsData& State)
{
    vec2 Result = {0.0f, 0.0f};
    return Result;
}

vec2
StandardGravity(const PhysicsData& State)
{
	vec2 Result = {0.0f, 0.0f};
	if(!State.Grounded)
	{
		Result.y = 9.81f;
	}
	return Result;
}

vec2 
ArrowGravity(const PhysicsData& State)
{
	vec2 Result = {0.0f, 0.0f};
	if(!State.Grounded)
	{
		Result.y = 5.81f;
	}
	return Result;
}

struct InputState
{
	bool RightPressed;
	bool LeftPressed;
	bool SwitchPressed;
	bool ActionPressed;
	bool Action2Pressed;
};

struct Main_Character
{
	static const float sMovtSpeed;
	static const float sJumpSpeed;

	Texture     PlayerOneRight;
	Texture     PlayerTwoRight;
	PhysicsData State;
	SDL_Rect    Aabb;
	int         HealthPoints;
	uint32_t    ArrowsHeld;
	int         PointsEarned;
	bool        IsP1Right;

	void Init();
	void UpdateAndRender(SDL_Renderer* Renderer);
	void MoveRight();
	void MoveLeft();
	void Jump();
	void SwitchSides();
};

enum ArrowState
{
	ArrowDrop,
	ArrowShot,
	ArrowGrounded
};

struct Arrow
{
	PhysicsData State;
	vec2        (*AccFuncPtr)(const PhysicsData&);
	SDL_Rect    Aabb;
	ArrowState  Flag;
	bool 		IsShotRight;
};

// this is both shield and sword
struct P2Weapon
{
	SDL_Rect Aabb;
	bool     InUse;
};

enum EnemyState
{
	ArrowKillable,
	SwordKillable,
	Charging
};

struct Enemy
{
	PhysicsData State;
	SDL_Rect Aabb;
	EnemyState EnemyType;
	bool DamageDealt;
};

// enemy which can attack back, look into unions
struct StrEnemy
{
	PhysicsData State;
	SDL_Rect    Aabb;
	uint32_t    ArrowsReqd;
	Uint32      AttackCounter;
	Uint32      ReloadCounter;
	Uint32      BulletCounter;
	bool        IsAlive;
	bool        IsAttacking;
	bool        DamageDealt;
};

struct Bullet
{
	PhysicsData State;
	SDL_Rect    Aabb;
	bool        IsActive;
};

struct HpPickup
{
	PhysicsData State;
	SDL_Rect    Aabb;
	uint32_t    Amount;
};