struct HUD
{
	Texture TextArrows;
	NumTexture NumArrows;
	Texture TextHP;
	NumTexture NumHP;
	SDL_Color  TextColor;
	Texture TextCharge;
	Texture BarLeftBoundary;
	Texture BarMid;
	Texture BarRightBoundary;
	Texture P1Press;
	Texture P2Press;
	Texture HighlightPress;
	Texture HighlightUnpress;
	Texture P1Unpress;
	Texture P2Unpress;
	Texture KeyName;
	Texture Narrator;
	Texture TextSkip;

	TTF_Font* TextFont;
	TTF_Font* KeyFont;

	void Init(SDL_Renderer* Renderer);
	void ShowControls(SDL_Renderer* Renderer, int TutStage, bool TutMode, bool IsP1Right);
	void RenderKey(SDL_Renderer* Renderer, char* c, int x, int y);
	void ShowSwitchStatus(SDL_Renderer* Renderer, bool p1switched, bool p2switched);
};

void
HUD::Init(SDL_Renderer* Renderer)
{
	TextColor = {0x38, 0x13, 0x56, 0xFF};
	SDL_Color Purple = {0x4B, 0x00, 0x82, 0xFF};
	NumArrows.NumColor = {0xa8, 0x58, 0x00, 0xFF};
	NumHP.NumColor = {0xa8, 0x58, 0x00, 0xFF};

	KeyFont = TTF_OpenFont("..\\assets\\fonts\\kr1.ttf", 36);
	TextFont = TTF_OpenFont("..\\assets\\fonts\\gomarice_kaiju_monster.ttf", 72);
	NumArrows.Font = TTF_OpenFont("..\\assets\\fonts\\gomarice_kaiju_monster.ttf", 72);
	NumHP.Font = TTF_OpenFont("..\\assets\\fonts\\gomarice_kaiju_monster.ttf", 72);

	Load_SolidText(Renderer, "Arrows: ", TextFont, TextColor, &TextArrows);
	Load_SolidText(Renderer, "HP: ", TextFont, TextColor, &TextHP);
	Load_SolidText(Renderer, "Charger: ", TextFont, TextColor, &TextCharge);
	Load_SolidText(Renderer, "Press t to skip tutorial.", TextFont, TextColor, &TextSkip);
	Load_SolidText(Renderer, "Switched!", TextFont, Purple, &Narrator);

	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\barHorizontal_blue_left.png", &BarLeftBoundary);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\barHorizontal_blue_mid.png", &BarMid);
	LoadImageWhiteColorKey(Renderer, "..\\assets\\graphics\\barHorizontal_blue_right.png", &BarRightBoundary);
	LoadImage(Renderer, "..\\assets\\graphics\\P2ButtonUnpressed.png", &P1Unpress);
	LoadImage(Renderer, "..\\assets\\graphics\\P2ButtonPress.png", &P1Press);
	LoadImage(Renderer, "..\\assets\\graphics\\P1ButtonPress.png", &P2Press);
	LoadImage(Renderer, "..\\assets\\graphics\\P1ButtonUnpressed.png", &P2Unpress);
	LoadImage(Renderer, "..\\assets\\graphics\\HighlightPress.png", &HighlightPress);
	LoadImage(Renderer, "..\\assets\\graphics\\HighlightUnpressed.png", &HighlightUnpress);

	int padding = 10;
	int start_x = (int)(0.1f*g_ResolutionWidth);
	int start_y = (int)(0.1f*g_ResolutionHeight);

	TextArrows.DrawRect.x = start_x;
	TextArrows.DrawRect.y = start_y;
	NumArrows.Tex.DrawRect.x = start_x + TextArrows.DrawRect.w;
	NumArrows.Tex.DrawRect.y = start_y;

	start_y += TextArrows.DrawRect.h + padding;
	TextHP.DrawRect.x = start_x;
	TextHP.DrawRect.y = start_y;
	NumHP.Tex.DrawRect.x = start_x + TextHP.DrawRect.w;
	NumHP.Tex.DrawRect.y = start_y;

	start_y += TextHP.DrawRect.h + padding;
	TextSkip.DrawRect.x = start_x;
	TextSkip.DrawRect.y = start_y;

	// Narrator.DrawRect.x = g_ResolutionWidth/2;
	// Narrator.DrawRect.y = 0.3f*g_ResolutionHeight;
}

// this is the tutorial!
void
HUD::ShowControls(SDL_Renderer* Renderer, int TutStage, bool TutMode, bool IsP1Right)
{
	SDL_Color key_color = {0x00, 0x00, 0x00, 0xFF};
	int padding = 20;
	int button_width = P1Press.DrawRect.w;
	int button_height = P1Press.DrawRect.h;
	int start_y = TextSkip.DrawRect.y + TextSkip.DrawRect.h;
	Uint32 wait_time = 200;

	int w_x = g_ResolutionWidth/2 - padding/2 - 2*button_width;
	int w_y = start_y;
	int a_x = w_x - button_width;
	int a_y = start_y + button_height;
	int s_x = w_x;
	int s_y = start_y + button_height;
	int d_x = w_x + button_width;
	int d_y = start_y + button_height;
	int q_x = a_x;
	int q_y = w_y;

	int i_x = g_ResolutionWidth/2 + padding/2 + button_width;
	int i_y = start_y;
	int j_x = i_x - button_width;
	int j_y = start_y + button_height;
	int k_x = i_x;
	int k_y = start_y + button_height;
	int l_x = i_x + button_width;
	int l_y = start_y + button_height;
	int o_x = l_x;
	int o_y = i_y;

	if(TutMode)
	{
		int key_press_padding = 5;

		TextSkip.Render(Renderer);
		local_persist bool pressed = true;
		local_persist Uint32 counter = SDL_GetTicks();
		if(GetMSElapsedSince(counter) > wait_time)
		{
			pressed = pressed ? false : true;
			counter = SDL_GetTicks();
		}
		if(TutStage == 0) // teach switch
		{
			if(pressed)
			{
				HighlightPress.Render(Renderer, w_x, w_y + key_press_padding);
				RenderKey(Renderer, "W", w_x, w_y + key_press_padding);

				HighlightPress.Render(Renderer, i_x, i_y + key_press_padding);
				RenderKey(Renderer, "I", i_x, i_y + key_press_padding);
			}
			else
			{
				HighlightUnpress.Render(Renderer, w_x, w_y);
				RenderKey(Renderer, "W", w_x, w_y);

				HighlightUnpress.Render(Renderer, i_x, i_y);
				RenderKey(Renderer, "I", i_x, i_y);
			}

			P2Unpress.Render(Renderer, q_x, q_y);
			RenderKey(Renderer, "Q", q_x, q_y);
			P2Unpress.Render(Renderer, a_x, a_y);
			RenderKey(Renderer, "A", a_x, a_y);
			P2Unpress.Render(Renderer, s_x, s_y);
			RenderKey(Renderer, "S", s_x, s_y);
			P2Unpress.Render(Renderer, d_x, d_y);
			RenderKey(Renderer, "D", d_x, d_y);

			P1Unpress.Render(Renderer, j_x, j_y);
			RenderKey(Renderer, "J", j_x, j_y);
			P1Unpress.Render(Renderer, k_x, k_y);
			RenderKey(Renderer, "K", k_x, k_y);
			P1Unpress.Render(Renderer, l_x, l_y);
			RenderKey(Renderer, "L", l_x, l_y);
		}
		else if(TutStage == 1) // teach movement
		{
			if(pressed)
			{
				if(IsP1Right)
				{
					HighlightPress.Render(Renderer, l_x, l_y + key_press_padding);
					RenderKey(Renderer, "L", l_x, l_y + key_press_padding);

					HighlightPress.Render(Renderer, a_x, a_y + key_press_padding);
					RenderKey(Renderer, "A", a_x, a_y + key_press_padding);
				}
				else
				{
					HighlightPress.Render(Renderer, j_x, j_y + key_press_padding);
					RenderKey(Renderer, "J", j_x, j_y + key_press_padding);

					HighlightPress.Render(Renderer, d_x, d_y + key_press_padding);
					RenderKey(Renderer, "D", d_x, d_y + key_press_padding);
				}
			}
			else
			{
				if(IsP1Right)
				{
					HighlightUnpress.Render(Renderer, l_x, l_y);
					RenderKey(Renderer, "L", l_x, l_y);

					HighlightUnpress.Render(Renderer, a_x, a_y);
					RenderKey(Renderer, "A", a_x, a_y);
				}
				else
				{
					HighlightUnpress.Render(Renderer, j_x, j_y);
					RenderKey(Renderer, "J", j_x, j_y);

					HighlightUnpress.Render(Renderer, d_x, d_y);
					RenderKey(Renderer, "D", d_x, d_y);
				}
			}

			P2Unpress.Render(Renderer, w_x, w_y);
			RenderKey(Renderer, "W", w_x, w_y);

			P2Unpress.Render(Renderer, s_x, s_y);
			RenderKey(Renderer, "S", s_x, s_y);

			P1Unpress.Render(Renderer, i_x, i_y);
			RenderKey(Renderer, "I", i_x, i_y);

			P1Unpress.Render(Renderer, k_x, k_y);
			RenderKey(Renderer, "K", k_x, k_y);

			if(IsP1Right)
			{
				// j and d unpressed
				P1Unpress.Render(Renderer, j_x, j_y);
				RenderKey(Renderer, "J", j_x, j_y);
				P2Unpress.Render(Renderer, d_x, d_y);
				RenderKey(Renderer, "D", d_x, d_y);
			}
			else
			{
				// a and l unpressed
				P1Unpress.Render(Renderer, l_x, l_y);
				RenderKey(Renderer, "L", l_x, l_y);
				P2Unpress.Render(Renderer, a_x, a_y);
				RenderKey(Renderer, "A", a_x, a_y);
			}
		}
		else if(TutStage == 2) // teach attacking
		{
			if(pressed)
			{
				HighlightPress.Render(Renderer, s_x, s_y + key_press_padding);
				RenderKey(Renderer, "S", s_x, s_y + key_press_padding);

				HighlightPress.Render(Renderer, k_x, k_y + key_press_padding);
				RenderKey(Renderer, "K", k_x, k_y + key_press_padding);

				HighlightPress.Render(Renderer, q_x, q_y + key_press_padding);
				RenderKey(Renderer, "Q", q_x, q_y + key_press_padding);
			}
			else
			{
				HighlightUnpress.Render(Renderer, s_x, s_y);
				RenderKey(Renderer, "S", s_x, s_y);

				HighlightUnpress.Render(Renderer, k_x, k_y);
				RenderKey(Renderer, "K", k_x, k_y);

				HighlightUnpress.Render(Renderer, q_x, q_y);
				RenderKey(Renderer, "Q", q_x, q_y);
			}

			P1Unpress.Render(Renderer, j_x, j_y);
			RenderKey(Renderer, "J", j_x, j_y);

			P1Unpress.Render(Renderer, i_x, i_y);
			RenderKey(Renderer, "I", i_x, i_y);
			
			P1Unpress.Render(Renderer, l_x, l_y);
			RenderKey(Renderer, "L", l_x, l_y);
			
			P2Unpress.Render(Renderer, w_x, w_y);
			RenderKey(Renderer, "W", w_x, w_y);

			P2Unpress.Render(Renderer, a_x, a_y);
			RenderKey(Renderer, "A", a_x, a_y);

			P2Unpress.Render(Renderer, d_x, d_y);
			RenderKey(Renderer, "D", d_x, d_y);
		}
	}
}

void
HUD::ShowSwitchStatus(SDL_Renderer* Renderer, bool P1SwitchPressed, bool P2SwitchPressed)
{
	int padding = 20;
	int button_width = P1Press.DrawRect.w;
	// int button_height = P1Press.DrawRect.h;
	int key_press_padding = 5;
	int w_x = g_ResolutionWidth/2 - padding/2 - 2*button_width;
	int w_y = g_ResolutionHeight/2;
	int i_x = g_ResolutionWidth/2 + padding/2 + button_width;
	int i_y = g_ResolutionHeight/2;

	if(P1SwitchPressed)
	{
		HighlightPress.Render(Renderer, i_x,  i_y + key_press_padding);
		RenderKey(Renderer, "I", i_x,  i_y + key_press_padding);
	}
	else
	{
		P1Unpress.Render(Renderer, i_x, i_y);
		RenderKey(Renderer, "I", i_x, i_y);
	}

    if(P2SwitchPressed)
	{
		HighlightPress.Render(Renderer, w_x,  w_y + key_press_padding);
		RenderKey(Renderer, "W", w_x,  w_y + key_press_padding);
	}
	else
	{
		P2Unpress.Render(Renderer, w_x, w_y);
		RenderKey(Renderer, "W", w_x, w_y);
	}

}
void
HUD::RenderKey(SDL_Renderer* Renderer, char* c, int x, int y)
{
	int w = P1Press.DrawRect.w;
	int h = P1Press.DrawRect.h;
	SDL_Color key_color = {0x00, 0x00, 0x00, 0xFF};
	Load_SolidText(Renderer, c, KeyFont, key_color, &KeyName);
	KeyName.Render(Renderer, 
				   x + w/2 - (KeyName.DrawRect.w)/2,
				   y + h/2 - (KeyName.DrawRect.h)/2);
	SDL_DestroyTexture(KeyName.Ptr);
}