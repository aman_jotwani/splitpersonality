struct Enemy_System
{
	static const uint32_t s_MaxBullets = 20;
	static const uint32_t s_MaxArrowKillables = 10;
	static const uint32_t s_MaxSwordKillables = 10;
	static const uint32_t s_StdSpawnIntervalMS = 20000;
	
	static const uint32_t s_GoombaSpawnIntervalMS = 10000;
	static const uint32_t s_GoombaAttackDuration = 300;
	static const uint32_t s_GoombaReloadDuration = 1000;
	static const uint32_t s_GoombaBulletSpace = 200;

	// these static values should be dependent on resolution instead
	static const float s_StdMovtSpeed;
	static const float s_ChargingMovtSpeed;
	static const float s_GoombaBulletSpeed;
	
	int      SpawnHeight;
	int      RightSpawnX;

	Texture  TexSwordEnemy;
	Texture  TexArrowEnemy;
	Texture  TexBullet;
	Pool<Enemy, 20> Enemies;
	StrEnemy Goomba;
	Bullet   BulletPool[s_MaxBullets];

	void Init(int PlayAreaWidth, int PlayAreaHeight, int GroundHeight);
	void UpdateAndRender(SDL_Renderer* Renderer);
	// void SpawnChargedEnemy();
	uint32_t GetNumArrowKillables();
	uint32_t GetNumSwordKillables();
	uint32_t GetNumChgEnemies();
	void SpawnStdEnemyGrounded(int X, bool IsArrowKillable);
	void SpawnEnemy(bool SpawnFromRight, EnemyState EnemyType);
	void SpawnIntermediateGoomba(bool SpawnFromRight, bool IsCharging);
	// void SpawnStdEnemyTimed(const Uint32 GameTimeCounter, Uint32 Interval, bool SpawnFromRight, bool IsCharging, bool IsArrowKillable);
	// void SpawnGoombaTimed(const Uint32 GameTimeCounter, Uint32 Interval, int Arrows, bool SpawnFromRight, bool IsCharging);
	void GoombaShootBullet();
};

const float Enemy_System::s_StdMovtSpeed = 5.0f;
const float Enemy_System::s_ChargingMovtSpeed = 5.5f;
const float Enemy_System::s_GoombaBulletSpeed = 7.5f;

void
Enemy_System::Init(int PlayAreaWidth, int PlayAreaHeight, int GroundHeight)
{
	RightSpawnX = PlayAreaWidth;
	SpawnHeight = PlayAreaHeight - GroundHeight - TexArrowEnemy.DrawRect.h;

	Enemy temp;
	temp.State = {{0.0f, 0.0f}, {0.0f, 0.0f}};
	temp.Aabb = {0};
	temp.EnemyType = ArrowKillable;
	temp.DamageDealt = false;
	Enemies.Init(temp);

	// init goomba
	Goomba.State = {{0.0f, 0.0f}, {0.0f, 0.0f}};
	Goomba.Aabb = {0, 0, TexArrowEnemy.DrawRect.w, TexArrowEnemy.DrawRect.h};
	Goomba.ArrowsReqd = 0;
	Goomba.AttackCounter = 0;
	Goomba.ReloadCounter = 0;
	Goomba.BulletCounter = 0;
	Goomba.IsAlive = false;
	Goomba.IsAttacking = false;
	Goomba.DamageDealt = false;

	for(int i = 0; i < s_MaxBullets; i++)
	{
		BulletPool[i].State = {{0.0f, 0.0f}, {0.0f, 0.0f}};
		BulletPool[i].Aabb = {0, 0, TexBullet.DrawRect.w, TexBullet.DrawRect.h};
		BulletPool[i].IsActive = false;
	}
}

void
Enemy_System::SpawnStdEnemyGrounded(int X, bool IsArrowKillable)
{
	Enemy temp;
	temp.State.Position = {X*g_PhyScale, SpawnHeight*g_PhyScale};
	temp.State.Velocity = {0.0f, 0.0f};
	temp.State.Grounded = true;
	UpdateAabb(temp.State.Position, &temp.Aabb);
	temp.DamageDealt = false;
	temp.EnemyType = IsArrowKillable ? ArrowKillable : SwordKillable;
	Enemies.Insert(temp);
}

// void
// Enemy_System::SpawnChargedEnemy()
// {
// 	if(GetMSElapsedSince(ChargeSpawnCounter) > s_ChargeSpawnIntervalMS)
// 	{
// 		bool type_choice = RandomDecision();
// 		bool dir_choice = RandomDecision();
// 		if(type_choice)
// 		{
// 			for(int i = 0; i < s_MaxArrowKillables; i++)
// 			{
// 				if(!ArrowKillables[i].IsAlive)
// 				{
// 					ArrowKillables[i].IsAlive = true;
// 					ArrowKillables[i].DamageDealt = false;
// 					if(dir_choice)
// 					{
// 						ArrowKillables[i].State.Position = {RightSpawnX * g_PhyScale,
// 															SpawnHeight * g_PhyScale};
// 						ArrowKillables[i].State.Velocity = {-s_ChargingMovtSpeed, 0.0f};
// 					}
// 					else
// 					{
// 						ArrowKillables[i].State.Position = {0.0f, 
// 															SpawnHeight * g_PhyScale};
// 						ArrowKillables[i].State.Velocity = {s_ChargingMovtSpeed, 0.0f};
// 					}
// 					UpdateAabb(ArrowKillables[i].State.Position, &ArrowKillables[i].Aabb);
// 					break;
// 				}
// 			}
// 		}
// 		else
// 		{
// 			for(int i = 0; i < s_MaxSwordKillables; i++)
// 			{
// 				if(!SwordKillables[i].IsAlive)
// 				{
// 					SwordKillables[i].IsAlive = true;
// 					SwordKillables[i].DamageDealt = false;
// 					if(dir_choice)
// 					{
// 						SwordKillables[i].State.Position = {RightSpawnX * g_PhyScale, 
// 															SpawnHeight * g_PhyScale};
// 						SwordKillables[i].State.Velocity = {-s_ChargingMovtSpeed, 0.0f};
// 					}
// 					else
// 					{
// 						SwordKillables[i].State.Position = {0.0f,
// 															SpawnHeight * g_PhyScale};
// 						SwordKillables[i].State.Velocity = {s_ChargingMovtSpeed, 0.0f};
// 					}
// 					UpdateAabb(SwordKillables[i].State.Position, &SwordKillables[i].Aabb);
// 					break;
// 				}
// 			}			
// 		}
// 		ChargeSpawnCounter = SDL_GetTicks();
// 	}
// }

void
Enemy_System::UpdateAndRender(SDL_Renderer* Renderer)
{
	SDL_SetTextureAlphaMod(TexSwordEnemy.Ptr, 0xFF);
	SDL_SetTextureAlphaMod(TexArrowEnemy.Ptr, 0xFF);

	// update enemy pool
	for(uint32_t i = 0; i < Enemies.Size(); i++)
	{
		Enemy* active_enemy = Enemies.GetIfActive(i);
		if(active_enemy)
		{
			if(active_enemy->Aabb.x > -100 && active_enemy->Aabb.x < RightSpawnX + 100)
			{
				Physics_System::Integrate(&active_enemy->State, NoAcceleration);
				UpdateAabb(active_enemy->State.Position, &active_enemy->Aabb);

				switch(active_enemy->EnemyType)
				{
					case ArrowKillable:
					{
						TexArrowEnemy.Render(Renderer, active_enemy->Aabb.x, active_enemy->Aabb.y);
					}
					break;

					case SwordKillable:
					{
						TexSwordEnemy.Render(Renderer, active_enemy->Aabb.x, active_enemy->Aabb.y);				
					}
					break;

					case Charging:
					{
						TexSwordEnemy.Render(Renderer, active_enemy->Aabb.x, active_enemy->Aabb.y);
					}
					break;
				}
			}
			else
			{
				Enemies.SetInactive(i);
			}
		}
	}

	if(Goomba.IsAlive)
	{
		Physics_System::Integrate(&Goomba.State, NoAcceleration);
		UpdateAabb(Goomba.State.Position, &Goomba.Aabb);
		if(Goomba.Aabb.x > -100 && Goomba.Aabb.x < RightSpawnX + 100)
		{
			if(Goomba.IsAttacking)
			{
				if(GetMSElapsedSince(Goomba.AttackCounter) > s_GoombaAttackDuration)
				{
					Goomba.IsAttacking = false;
					Goomba.ReloadCounter = SDL_GetTicks();
					Goomba.BulletCounter = 0;
				}
				else if(GetMSElapsedSince(Goomba.BulletCounter) > s_GoombaBulletSpace)
				{
					GoombaShootBullet();
					Goomba.BulletCounter = SDL_GetTicks();
				}
			}
			else
			{
				if(GetMSElapsedSince(Goomba.ReloadCounter) > s_GoombaReloadDuration)
				{
					Goomba.IsAttacking = true;
					Goomba.AttackCounter = SDL_GetTicks();
					Goomba.BulletCounter = SDL_GetTicks();
				}
			}
			TexArrowEnemy.Render(Renderer, Goomba.Aabb.x, Goomba.Aabb.y);
		}
		else
		{
			Goomba.IsAlive = false;
		}
	}

	for(int BulletIndex = 0; BulletIndex < s_MaxBullets; BulletIndex++)
	{
		if(BulletPool[BulletIndex].IsActive)
		{
			Physics_System::Integrate(&BulletPool[BulletIndex].State, NoAcceleration);
			UpdateAabb(BulletPool[BulletIndex].State.Position, &BulletPool[BulletIndex].Aabb);
			TexBullet.Render(Renderer, BulletPool[BulletIndex].Aabb.x, BulletPool[BulletIndex].Aabb.y);
		}
	}
}

void
Enemy_System::GoombaShootBullet()
{
	for(int i = 0; i < s_MaxBullets; i++)
	{
		if(!BulletPool[i].IsActive)
		{
			BulletPool[i].IsActive = true;
			BulletPool[i].State.Position = Goomba.State.Position;
			if(Goomba.State.Velocity.x > 0.0f)
			{
				BulletPool[i].State.Velocity = {s_GoombaBulletSpeed, 0.0f};
			}
			else
			{
				BulletPool[i].State.Velocity = {-s_GoombaBulletSpeed, 0.0f};
			}
			UpdateAabb(BulletPool[i].State.Position, &BulletPool[i].Aabb);
			break;
		}
	}
}

void
Enemy_System::SpawnEnemy(bool SpawnFromRight, EnemyState EnemyType)
{
	Enemy temp;

	temp.State.Position.x = SpawnFromRight ? RightSpawnX * g_PhyScale : 0.0f;
	temp.State.Position.y = SpawnHeight * g_PhyScale;
	UpdateAabb(temp.State.Position, &temp.Aabb);

	temp.State.Velocity.x = SpawnFromRight ? -1.0f : 1.0f;
	temp.State.Velocity.y = 0.0f;
	
	if(EnemyType == Charging)
	{
		temp.Aabb.w = TexSwordEnemy.DrawRect.w;
		temp.Aabb.h = TexSwordEnemy.DrawRect.h;

		temp.State.Velocity.x *= s_ChargingMovtSpeed;
	}
	else if(EnemyType == ArrowKillable)
	{
		temp.Aabb.w = TexArrowEnemy.DrawRect.w;
		temp.Aabb.h = TexArrowEnemy.DrawRect.h;
		
		temp.State.Velocity.x *= s_StdMovtSpeed;
	}
	else if(EnemyType == SwordKillable)
	{
		temp.Aabb.w = TexSwordEnemy.DrawRect.w;
		temp.Aabb.h = TexSwordEnemy.DrawRect.h;

		temp.State.Velocity.x *= s_StdMovtSpeed;		
	}

	temp.DamageDealt = false;
	temp.EnemyType = EnemyType;

	Enemies.Insert(temp);
}

void
Enemy_System::SpawnIntermediateGoomba(bool SpawnFromRight, bool IsCharging)
{
	if(SpawnFromRight)
	{
		Goomba.State.Position = {RightSpawnX * g_PhyScale,
								SpawnHeight * g_PhyScale};
		Goomba.State.Velocity = {-1.0f, 0.0f};
	}
	else
	{
		Goomba.State.Position = {0.0f, 
								SpawnHeight * g_PhyScale};
        Goomba.State.Velocity = {1.0f, 0.0f};								
	}
	UpdateAabb(Goomba.State.Position, &Goomba.Aabb);

	if(IsCharging)
	{
		Goomba.State.Velocity.x *= s_ChargingMovtSpeed;
	}
	else
	{
		Goomba.State.Velocity.x *= (s_StdMovtSpeed - 3.0f); 
	}

	Goomba.ArrowsReqd = 2;
	Goomba.AttackCounter = 0;
	Goomba.ReloadCounter = SDL_GetTicks();
	Goomba.BulletCounter = 0;
	Goomba.IsAlive = true;
	Goomba.IsAttacking = false;
	Goomba.DamageDealt = false;		
}

// NOTE: the following functions are used by ai system and could be converted to just one iteration over the enemy pool
// instead of three
uint32_t
Enemy_System::GetNumArrowKillables()
{
	uint32_t Result = 0;
	for(uint32_t i = 0; i < Enemies.Size(); i++)
	{
		Enemy* active_enemy = Enemies.GetIfActive(i);
		if(active_enemy && active_enemy->EnemyType == ArrowKillable)		
		{
			Result += 1;			
		}
	}
	return Result;
}

uint32_t
Enemy_System::GetNumSwordKillables()
{
	uint32_t Result = 0;
	for(uint32_t i = 0; i < Enemies.Size(); i++)
	{
		Enemy* active_enemy = Enemies.GetIfActive(i);
		if(active_enemy && active_enemy->EnemyType == SwordKillable)
		{
			Result += 1;			
		}
	}
	return Result;
}

uint32_t
Enemy_System::GetNumChgEnemies()
{
	uint32_t Result = 0;
	for(uint32_t i = 0; i < Enemies.Size(); i++)
	{
		Enemy* active_enemy = Enemies.GetIfActive(i);
		if(active_enemy && active_enemy->EnemyType == Charging)
		{
			Result += 1;
		}
	}
	return Result;
}