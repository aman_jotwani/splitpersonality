struct Logger
{
    static const uint32_t  s_LogBufferSize = 1024;
    uint32_t  HeightFromTop;

    void Init();
    void ScreenLog(SDL_Renderer* Renderer, const char* FormattedStr, ... );
    void SpecificLog(SDL_Renderer* Renderer, int x, int y, const char* FormattedStr, ... );
private:
    SDL_Color DebugColor;
    Texture   LogTexture;
    TTF_Font* DebugFont;
    char      DebugBuffer[s_LogBufferSize];
};

void
Logger::Init()
{
	DebugColor = {0xFF, 0x00, 0x00, 0xFF};
	DebugFont = TTF_OpenFont("..\\assets\\fonts\\SourceCodePro-ExtraLight.ttf", 18);
	// memset(DebugBuffer, 0, s_LogBufferSize);
	uint32_t HeightFromTop = 200;
}

void
Logger::ScreenLog(SDL_Renderer* Renderer, const char* FormattedStr, ... )
{
    // DebugFont = TTF_OpenFont("..\\assets\\fonts\\SourceCodePro-ExtraLight.ttf", 18);

	if(strlen(FormattedStr) > s_LogBufferSize)
	{
		ConsoleLog("WARNING: String size too big(> s_LogBufferSize) to display on screen.\n");
	}

    va_list Args;
    va_start(Args, FormattedStr);
    vsnprintf(DebugBuffer, s_LogBufferSize, FormattedStr, Args);
    va_end(Args);
    
    Load_SolidText(Renderer, DebugBuffer, DebugFont, DebugColor, &LogTexture);
  
    LogTexture.Render(Renderer, 100, HeightFromTop);
    
    HeightFromTop += LogTexture.DrawRect.h;
    
    SDL_DestroyTexture(LogTexture.Ptr);
}

void
Logger::SpecificLog(SDL_Renderer* Renderer, int x, int y, const char* FormattedStr, ... )
{
    // DebugFont = TTF_OpenFont("..\\assets\\fonts\\SourceCodePro-ExtraLight.ttf", 18);

    if(strlen(FormattedStr) > s_LogBufferSize)
    {
        ConsoleLog("WARNING: String size too big(> s_LogBufferSize) to display on screen.\n");
    }

    va_list Args;
    va_start(Args, FormattedStr);
    vsnprintf(DebugBuffer, s_LogBufferSize, FormattedStr, Args);
    va_end(Args);
    
    Load_SolidText(Renderer, DebugBuffer, DebugFont, DebugColor, &LogTexture);
  
    LogTexture.Render(Renderer, x, y);
        
    SDL_DestroyTexture(LogTexture.Ptr);
}