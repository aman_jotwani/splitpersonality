struct Powerup_System
{
	static const uint32_t s_SwordAttackDurationMS = 200;
	static const uint32_t s_ShieldDuration = 300;

	// dependent on resolution?
	static const float s_ShootSpeed;
	static const float s_SlowDownSpeed;
	static const float s_ChargedSpeed;

	Pool<HpPickup, 20> HpDrops;
	Pool<Arrow, 10> Arrows;

	Texture TexSwordRight;
	Texture TexArrowRight;
	Texture TexShieldRight;
	Texture TexHp;
	NumTexture HpPickupText;
	P2Weapon TheSword;
	P2Weapon TheShield;
	Uint32  SwordExpiryCounter;

	void Init();
	void ShootArrow(uint32_t* ArrowsHeld, int PlayerX, float SpawnY, int PlayerWidth, bool IsP1Right);
	void ShootChargedArrow(uint32_t* ArrowsHeld, int PlayerX, float SpawnY, int PlayerWidth, bool IsP1Right);
	void PopArrowFromEnemy(int x, int y);
	void SpawnHp(int x, int y);
	void SpawnArrowOnGround(int X, int Y);
	void SpawnArrowMidair();
	uint32_t GetGroundedArrows();
	void UpdateArrows(SDL_Renderer*);
	void UpdateSword(SDL_Renderer*, int PlayerX, int PlayerY, int PlayerWidth, bool IsP1Right);
	void UpdateShield(SDL_Renderer*, int PlayerX, int PlayerY, int PlayerWidth, bool IsP1Right);
	void UpdateAndRender(SDL_Renderer*, int PlayAreaWidth, int PlayerX, int PlayerY, int PlayerWidth, bool IsP1Right);
};

const float Powerup_System::s_ShootSpeed = 35.0f;
const float Powerup_System::s_SlowDownSpeed = 2.0f;

void
Powerup_System::Init()
{
	TheSword.InUse = false;
	TheSword.Aabb = {0, 0, TexSwordRight.DrawRect.w, TexSwordRight.DrawRect.h};

	TheShield.InUse = false;
	TheShield.Aabb = {0, 0, TexShieldRight.DrawRect.w, TexShieldRight.DrawRect.h};

	// initialize arrow pool
	Arrow init_arrow;
	init_arrow.State = {{0.0f, 0.0f}, {0.0f, 0.0f}, false};
	init_arrow.Aabb = {0, 0, TexArrowRight.DrawRect.w, TexArrowRight.DrawRect.h};
	init_arrow.Flag = ArrowDrop;
	init_arrow.IsShotRight = true;
	init_arrow.AccFuncPtr = NoAcceleration;
	Arrows.Init(init_arrow);

	HpPickup Obj;
	Obj.State = {{0.0f, 0.0f}, {0.0f, 0.0f}};
	Obj.Aabb = {0, 0, TexHp.DrawRect.w, TexHp.DrawRect.h};
	Obj.Amount = 0;
	HpDrops.Init(Obj);

	HpPickupText.Init("..\\assets\\fonts\\kr1.ttf", 0, 40, 0, 0, {0x00, 0xFF, 0x00, 0xFF});

	SwordExpiryCounter = 0;
}

uint32_t
Powerup_System::GetGroundedArrows()
{
	uint32_t Result = 0;
	for(size_t i = 0; i < Arrows.Size(); i++)
	{
		Arrow* active_arrow = Arrows.GetIfActive(i);
		if(active_arrow && active_arrow->Flag == ArrowGrounded)
		{
			Result += 1;
		}
	}
	return Result;
}

void
Powerup_System::SpawnArrowOnGround(int X, int Y)
{
	Arrow grounded_arrow;
	grounded_arrow.State.Position = {X*g_PhyScale, Y*g_PhyScale};
	grounded_arrow.State.Velocity = {0.0f, 0.0f};
	grounded_arrow.State.Grounded = true;
	UpdateAabb(grounded_arrow.State.Position, &(grounded_arrow.Aabb));
	grounded_arrow.Aabb.w = TexArrowRight.DrawRect.w;
	grounded_arrow.Aabb.h = TexArrowRight.DrawRect.h;
	grounded_arrow.IsShotRight = false;
	grounded_arrow.AccFuncPtr = NoAcceleration;
	
	Arrows.Insert(grounded_arrow);
}

void
Powerup_System::ShootArrow(uint32_t* ArrowsHeld, int PlayerX, float SpawnY, int PlayerWidth, bool ShotRight)
{
	if((*ArrowsHeld) > 0)
	{
		(*ArrowsHeld) -= 1;
		int Multiplier = ShotRight ? 1 : -1;
		float SpawnX = ShotRight ? (PlayerX + PlayerWidth)*g_PhyScale : (PlayerX - TexArrowRight.DrawRect.w)*g_PhyScale;
		
		Arrow shot_arrow;
		shot_arrow.State.Position = {SpawnX, SpawnY};
		shot_arrow.State.Velocity = {Multiplier*s_ShootSpeed, 0.0f};
		shot_arrow.State.Grounded = false;
		UpdateAabb(shot_arrow.State.Position, &(shot_arrow.Aabb));
		shot_arrow.Aabb.w = TexArrowRight.DrawRect.w;
		shot_arrow.Aabb.h = TexArrowRight.DrawRect.h;
		shot_arrow.AccFuncPtr = ArrowGravity;
		shot_arrow.Flag = ArrowShot;
		shot_arrow.IsShotRight = ShotRight;

		Arrows.Insert(shot_arrow);

	}
}

void
Powerup_System::SpawnArrowMidair()
{
	Arrow midair_arrow;
	midair_arrow.State.Position = {GetRandomNum(TexArrowRight.DrawRect.w, 
												g_ResolutionWidth - 2*TexArrowRight.DrawRect.w) * g_PhyScale, 
								   20};
	midair_arrow.State.Velocity = {0.0f, 0.0f};
	midair_arrow.State.Grounded = false;
	UpdateAabb(midair_arrow.State.Position, &(midair_arrow.Aabb));
	midair_arrow.Aabb.w = TexArrowRight.DrawRect.w;
	midair_arrow.Aabb.h = TexArrowRight.DrawRect.h;
	midair_arrow.AccFuncPtr = ArrowGravity;
	midair_arrow.Flag = ArrowDrop;
	midair_arrow.IsShotRight = false; // doesnt matter

	Arrows.Insert(midair_arrow);
}

void
Powerup_System::UpdateArrows(SDL_Renderer* Renderer)
{
	for(uint32_t ArrowIndex = 0; ArrowIndex < Arrows.Size(); ArrowIndex++)
	{
		Arrow* active_arrow = Arrows.GetIfActive(ArrowIndex);
		if(active_arrow)
		{
			Physics_System::Integrate(&active_arrow->State, active_arrow->AccFuncPtr);				

			UpdateAabb(active_arrow->State.Position, &active_arrow->Aabb);
			
			if(active_arrow->IsShotRight)
			{
				TexArrowRight.Render(Renderer, active_arrow->Aabb.x, active_arrow->Aabb.y);			
			}
			else
			{
				TexArrowRight.RenderFlipped(Renderer, active_arrow->Aabb.x, active_arrow->Aabb.y);
			}
		}
	}
}

void
Powerup_System::UpdateSword(SDL_Renderer* Renderer, int PlayerX, int PlayerY, int PlayerWidth, bool IsP1Right)
{
	local_persist const int adjustment = 10;
	if(TheSword.InUse)
	{
		if(GetMSElapsedSince(SwordExpiryCounter) > s_SwordAttackDurationMS)
		{
			TheSword.InUse = false;
			// P2Input.ActionPressed = false;
		}
		else
		{
			if(IsP1Right)
			{
				TheSword.Aabb.x = PlayerX - TheSword.Aabb.w + adjustment;
				TheSword.Aabb.y = PlayerY;
				TexSwordRight.RenderFlipped(Renderer, TheSword.Aabb.x, TheSword.Aabb.y);
			}
			else
			{
				TheSword.Aabb.x = PlayerX + PlayerWidth - adjustment;
				TheSword.Aabb.y = PlayerY;
				TexSwordRight.Render(Renderer, TheSword.Aabb.x, TheSword.Aabb.y);
			}
		}
	}
}

// void
// Powerup_System::ShootChargedArrow(uint32_t* ArrowsHeld, int PlayerX, float SpawnY, int PlayerWidth, bool ShotRight)
// {
// 	if((*ArrowsHeld) > 0)
// 	{
// 		(*ArrowsHeld) -= 1;
// 		int Multiplier = ShotRight ? 1 : -1;
// 		float SpawnX = ShotRight ? (PlayerX + PlayerWidth)*g_PhyScale : (PlayerX - TexArrowRight.DrawRect.w)*g_PhyScale;
// 		for(int ArrowIndex = 0; ArrowIndex < s_MaxArrows; ArrowIndex++)
// 		{
// 			if(active_arrow->Flag == ArrowInactive)
// 			{
// 				ArrowPool[ArrowIndex].Flag = ArrowCharged;
// 				ArrowPool[ArrowIndex].IsShotRight = ShotRight;
// 				ArrowPool[ArrowIndex].State.Position = {SpawnX, SpawnY};
// 				ArrowPool[ArrowIndex].State.Velocity = {Multiplier*s_ChargedSpeed, 0.0f};
// 				ArrowPool[ArrowIndex].State.Grounded = false;
// 				ArrowPool[ArrowIndex].AccFuncPtr = NoAcceleration;
// 				UpdateAabb(ArrowPool[ArrowIndex].State.Position, &ArrowPool[ArrowIndex].Aabb);
// 				break;
// 			}
// 		}
// 	}
// }

void
Powerup_System::UpdateShield(SDL_Renderer* Renderer, int PlayerX, int PlayerY, int PlayerWidth, bool IsP1Right)
{
	if(TheShield.InUse)
	{
		if(IsP1Right)
		{
			TheShield.Aabb.x = PlayerX - TheShield.Aabb.w;
			TheShield.Aabb.y = PlayerY;
			TexShieldRight.RenderFlipped(Renderer, PlayerX - TexShieldRight.DrawRect.w, PlayerY);
		}
		else
		{
			TheShield.Aabb.x = PlayerX + PlayerWidth;
			TheShield.Aabb.y = PlayerY;
			TexShieldRight.Render(Renderer, PlayerX + PlayerWidth, PlayerY);
		}
	}
}
void
Powerup_System::SpawnHp(int x, int y)
{
	HpPickup Obj;
	Obj.Aabb = {x, y, TexHp.DrawRect.w, TexHp.DrawRect.h};
	Obj.State.Position = {Obj.Aabb.x * g_PhyScale, Obj.Aabb.y * g_PhyScale};
	Obj.State.Velocity = {0.1f, -10.f};
	Obj.State.Grounded = false;
	Obj.Amount = GetRandomInt(1, 10);
	HpDrops.Insert(Obj);
}

void 
Powerup_System::PopArrowFromEnemy(int x, int y)
{
	Arrow popped_arrow;
	popped_arrow.State.Position = {x*g_PhyScale, y*g_PhyScale};
	popped_arrow.State.Velocity = {0.0f, -10.0f};
	popped_arrow.State.Grounded = false;
	UpdateAabb(popped_arrow.State.Position, &(popped_arrow.Aabb));
	popped_arrow.Aabb.w = TexArrowRight.DrawRect.w;
	popped_arrow.Aabb.h = TexArrowRight.DrawRect.h;
	popped_arrow.AccFuncPtr = ArrowGravity;
	popped_arrow.Flag = ArrowDrop;
	popped_arrow.IsShotRight = true; // doesnt matter

	Arrows.Insert(popped_arrow);
}

void
Powerup_System::UpdateAndRender(SDL_Renderer* Renderer, int PlayAreaWidth, int PlayerX, int PlayerY, int PlayerWidth, bool IsP1Right)
{
	UpdateArrows(Renderer);
	UpdateSword(Renderer, PlayerX, PlayerY, PlayerWidth, IsP1Right);
	UpdateShield(Renderer, PlayerX, PlayerY, PlayerWidth, IsP1Right);

	for(size_t i = 0; i < HpDrops.Size(); i++)
	{
		HpPickup* curr_hpdrop = HpDrops.GetIfActive(i);
		if(curr_hpdrop)
		{
			Physics_System::Integrate(&curr_hpdrop->State, StandardGravity);
			UpdateAabb(curr_hpdrop->State.Position, &curr_hpdrop->Aabb);
			
			TexHp.Render(Renderer, curr_hpdrop->Aabb.x, curr_hpdrop->Aabb.y);
			
			HpPickupText.Render(Renderer, 
							    curr_hpdrop->Aabb.x + TexHp.DrawRect.w/2, 
								curr_hpdrop->Aabb.y - HpPickupText.Tex.DrawRect.h, 
								curr_hpdrop->Amount);
		}
	}
}